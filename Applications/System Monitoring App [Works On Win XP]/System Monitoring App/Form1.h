#pragma once

namespace SystemMonitoringApp {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected: 
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::Panel^  panel2;
	private: System::Windows::Forms::Panel^  panel3;
	private: System::Diagnostics::PerformanceCounter^  performanceCounter1;
	private: System::Diagnostics::PerformanceCounter^  performanceCounter2;
	private: System::Diagnostics::PerformanceCounter^  performanceCounter3;
	private: System::Windows::Forms::NotifyIcon^  notifyIcon1;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip1;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::ToolStripMenuItem^  showToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  hideToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->performanceCounter1 = (gcnew System::Diagnostics::PerformanceCounter());
			this->performanceCounter2 = (gcnew System::Diagnostics::PerformanceCounter());
			this->performanceCounter3 = (gcnew System::Diagnostics::PerformanceCounter());
			this->notifyIcon1 = (gcnew System::Windows::Forms::NotifyIcon(this->components));
			this->contextMenuStrip1 = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->showToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->hideToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->performanceCounter1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->performanceCounter2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->performanceCounter3))->BeginInit();
			this->contextMenuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(391, 61);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(13, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"0";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(391, 180);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(13, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"0";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(391, 305);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(13, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"0";
			this->label3->Click += gcnew System::EventHandler(this, &Form1::label3_Click);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(12, 49);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(105, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"Processor Usage [%]";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(12, 180);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(95, 13);
			this->label5->TabIndex = 4;
			this->label5->Text = L"Memory Usage [%]";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(12, 305);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(71, 13);
			this->label6->TabIndex = 5;
			this->label6->Text = L"Disk Time [%]";
			// 
			// panel1
			// 
			this->panel1->BackColor = System::Drawing::Color::White;
			this->panel1->Location = System::Drawing::Point(123, 12);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(250, 101);
			this->panel1->TabIndex = 6;
			this->panel1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Form1::panel1_Paint);
			// 
			// panel2
			// 
			this->panel2->BackColor = System::Drawing::Color::White;
			this->panel2->Location = System::Drawing::Point(123, 135);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(250, 101);
			this->panel2->TabIndex = 7;
			this->panel2->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Form1::panel2_Paint);
			// 
			// panel3
			// 
			this->panel3->BackColor = System::Drawing::Color::White;
			this->panel3->Location = System::Drawing::Point(123, 258);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(250, 101);
			this->panel3->TabIndex = 8;
			this->panel3->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Form1::panel3_Paint);
			// 
			// performanceCounter1
			// 
			this->performanceCounter1->CategoryName = L"Procesor";
			this->performanceCounter1->CounterName = L"Czas procesora (%)";
			this->performanceCounter1->InstanceName = L"_Total";
			// 
			// performanceCounter2
			// 
			this->performanceCounter2->CategoryName = L"Pami��";
			this->performanceCounter2->CounterName = L"Zadeklarowane bajty w u�yciu (%)";
			// 
			// performanceCounter3
			// 
			this->performanceCounter3->CategoryName = L"Dysk fizyczny";
			this->performanceCounter3->CounterName = L"Czas dysku (%)";
			this->performanceCounter3->InstanceName = L"_Total";
			// 
			// notifyIcon1
			// 
			this->notifyIcon1->ContextMenuStrip = this->contextMenuStrip1;
			this->notifyIcon1->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"notifyIcon1.Icon")));
			this->notifyIcon1->Text = L"System Monitoring App";
			this->notifyIcon1->Visible = true;
			// 
			// contextMenuStrip1
			// 
			this->contextMenuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->showToolStripMenuItem, 
				this->hideToolStripMenuItem, this->exitToolStripMenuItem});
			this->contextMenuStrip1->Name = L"contextMenuStrip1";
			this->contextMenuStrip1->Size = System::Drawing::Size(104, 70);
			// 
			// showToolStripMenuItem
			// 
			this->showToolStripMenuItem->Name = L"showToolStripMenuItem";
			this->showToolStripMenuItem->Size = System::Drawing::Size(103, 22);
			this->showToolStripMenuItem->Text = L"Show";
			this->showToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::showToolStripMenuItem_Click);
			// 
			// hideToolStripMenuItem
			// 
			this->hideToolStripMenuItem->Name = L"hideToolStripMenuItem";
			this->hideToolStripMenuItem->Size = System::Drawing::Size(103, 22);
			this->hideToolStripMenuItem->Text = L"Hide";
			this->hideToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::hideToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(103, 22);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::exitToolStripMenuItem_Click);
			// 
			// timer1
			// 
			this->timer1->Interval = 500;
			this->timer1->Tick += gcnew System::EventHandler(this, &Form1::timer1_Tick);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(458, 371);
			this->Controls->Add(this->panel3);
			this->Controls->Add(this->panel2);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"Form1";
			this->Text = L"System Monitoring App";
			this->Shown += gcnew System::EventHandler(this, &Form1::Form1_Shown);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^ >(this->performanceCounter1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^ >(this->performanceCounter2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^ >(this->performanceCounter3))->EndInit();
			this->contextMenuStrip1->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: System::Int32 x;
	private: array<System::Int32>^ Y1;
	private: array<System::Int32>^ Y2;
	private: array<System::Int32>^ Y3;
	private: System::Void DrawChart() 
			 {
				 x++;
				 System::Single YSingle;

				 YSingle = Convert::ToSingle(label1->Text);
				 Y1[x] = (System::Int32)YSingle;
				 panel1->Refresh();

				 YSingle = Convert::ToSingle(label2->Text);
				 Y2[x] = (System::Int32)YSingle;
				 panel2->Refresh();

				 YSingle = Convert::ToSingle(label3->Text);
				 Y3[x] = (System::Int32)YSingle;
				 panel3->Refresh();


			 }


	private: System::Void label3_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {

			 label1->Text = performanceCounter1->NextValue().ToString();
			 label2->Text = performanceCounter2->NextValue().ToString();
			 label3->Text = performanceCounter3->NextValue().ToString();

			 DrawChart();
		 }
private: System::Void showToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			 timer1->Enabled=true;
			 this->Show();
		 }
private: System::Void Form1_Shown(System::Object^  sender, System::EventArgs^  e) {

			 this->Hide();
			 x=0;
			 Y1 = gcnew array<System::Int32>(260);
			 Y2 = gcnew array<System::Int32>(260);
			 Y3 = gcnew array<System::Int32>(260);
		 }
private: System::Void hideToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			this->Hide();
			timer1->Enabled=false;
		 }
private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			 Close();
		 }
private: System::Void panel1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {

			 Pen^ LinePen1 = gcnew Pen(System::Drawing::Color::Blue);
			 if (x>250)
			 {
				 x=0;
				 e->Graphics->Clear(System::Drawing::Color::White);
			 }

			 for (System::Int32 x1=1; x1<x; x1++)
			 {
				 e->Graphics->DrawLine(LinePen1, x1-1, 100 - Y1[x1-1], x1, 100 - Y1[x1]);
			 }
		 }


private: System::Void panel2_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {

			 Pen^ LinePen1 = gcnew Pen(System::Drawing::Color::Blue);
			 if (x>250)
			 {
				 x=0;
				 e->Graphics->Clear(System::Drawing::Color::White);
			 }

			 for (System::Int32 x1=1; x1<x; x1++)
			 {
				 e->Graphics->DrawLine(LinePen1, x1-1, 100 - Y2[x1-1], x1, 100 - Y2[x1]);
			 }
		 }



private: System::Void panel3_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {


			 Pen^ LinePen1 = gcnew Pen(System::Drawing::Color::Blue);
			 if (x>250)
			 {
				 x=0;
				 e->Graphics->Clear(System::Drawing::Color::White);
			 }

			 for (System::Int32 x1=1; x1<x; x1++)
			 {
				 e->Graphics->DrawLine(LinePen1, x1-1, 100 - Y3[x1-1], x1, 100 - Y3[x1]);
			 }
		 }
};
}

