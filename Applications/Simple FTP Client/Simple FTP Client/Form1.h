#pragma once

namespace SimpleFTPClient {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Net;
	using namespace System::IO;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected: 
	private: System::Windows::Forms::ToolStripMenuItem^  infoToolStripMenuItem;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::TextBox^  textBox5;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->infoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->infoToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(376, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// infoToolStripMenuItem
			// 
			this->infoToolStripMenuItem->Name = L"infoToolStripMenuItem";
			this->infoToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->infoToolStripMenuItem->Text = L"Info";
			this->infoToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::infoToolStripMenuItem_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(12, 72);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(352, 251);
			this->textBox1->TabIndex = 1;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(11, 341);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(352, 20);
			this->textBox2->TabIndex = 2;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(11, 367);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 52);
			this->button1->TabIndex = 3;
			this->button1->Text = L"Catalog";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(72, 34);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(100, 20);
			this->textBox3->TabIndex = 4;
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(264, 34);
			this->textBox4->Name = L"textBox4";
			this->textBox4->PasswordChar = '*';
			this->textBox4->Size = System::Drawing::Size(100, 20);
			this->textBox4->TabIndex = 5;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(8, 37);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(58, 13);
			this->label1->TabIndex = 6;
			this->label1->Text = L"Username:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(193, 37);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(56, 13);
			this->label2->TabIndex = 7;
			this->label2->Text = L"Password:";
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(92, 367);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(93, 23);
			this->button2->TabIndex = 8;
			this->button2->Text = L"Download File";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(217, 370);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(147, 20);
			this->textBox5->TabIndex = 9;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(8, 326);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(169, 13);
			this->label3->TabIndex = 10;
			this->label3->Text = L"Enter ftp adress like: ftp.icm.edu.pl";
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(92, 396);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(93, 23);
			this->button3->TabIndex = 11;
			this->button3->Text = L"Upload File";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(376, 420);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->textBox5);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Simple FTP Client";
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void infoToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				 
			
			String^ message = "This is simple FTP Client. Author: Artur Wieczorek";
			String^ caption = "FTProgram 1.0";
			MessageBoxButtons buttons = MessageBoxButtons::OK;	

          // Displays the MessageBox.
          MessageBox::Show( this, message, caption, buttons );
		  			 
			 }


	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

				 System::String^ username;
				 System::String^ password;
				 username=textBox3->Text;
				 password=textBox4->Text;

				 Uri^ adres = gcnew Uri("ftp://"+textBox2->Text);
				 FtpWebRequest^ req = dynamic_cast<FtpWebRequest^>(WebRequest::Create(adres));
				 req->Credentials=gcnew NetworkCredential(username, password);
				 req->Method=WebRequestMethods::Ftp::ListDirectoryDetails;
				 FtpWebResponse^ resp;
				 resp=dynamic_cast<FtpWebResponse^>(req->GetResponse());
				 Stream^ resp_stream = resp->GetResponseStream();
				 StreamReader^ reader = gcnew StreamReader(resp_stream);
				 String^ linia;
				 textBox1->Clear();
				 while (!reader->EndOfStream){
					 linia=reader->ReadLine();
				     textBox1->AppendText(linia+System::Environment::NewLine);
				 }

			 }
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {

				 System::String^ username;
				 System::String^ password;
				 username=textBox3->Text;
				 password=textBox4->Text;

			 saveFileDialog1->Filter = "All Kind of Files(*.*)|*.*";
			 if (saveFileDialog1->ShowDialog()==System::Windows::Forms::DialogResult::OK){
			 
				 Uri^ adres=gcnew Uri("ftp://"+textBox2->Text+"/"+ textBox5->Text);
				 FtpWebRequest^ req = dynamic_cast<FtpWebRequest^>(WebRequest::Create(adres));
				 req->Credentials=gcnew NetworkCredential(username, password);
				 req->Method=WebRequestMethods::Ftp::DownloadFile;
				 FtpWebResponse^ resp;
				 resp=dynamic_cast<FtpWebResponse^>(req->GetResponse());
				 Stream^ resp_stream = resp->GetResponseStream();
				 FileStream^ stru_plik = gcnew FileStream(saveFileDialog1->FileName,FileMode::Create);
				 int ile_bajtow;
				 array<Byte>^ bufor = gcnew array<Byte>(1024);
				 do {
					ile_bajtow=resp_stream->Read(bufor,0,bufor->Length);
					stru_plik->Write(bufor,0,ile_bajtow);
				 }while (ile_bajtow!=0);
				 stru_plik->Flush();
				 stru_plik->Close();
				 resp_stream->Close();
				 resp->Close();



			 }


		 }
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {

				 System::String^ username;
				 System::String^ password;
				 username=textBox3->Text;
				 password=textBox4->Text;

			 if (openFileDialog1->ShowDialog()==System::Windows::Forms::DialogResult::OK){
			 if(openFileDialog1->FileName=="")
				 return;
			 Uri^ adres = gcnew Uri("ftp://"+textBox2->Text+"/"+Path::GetFileName(openFileDialog1->FileName));
			 FtpWebRequest^ req = dynamic_cast<FtpWebRequest^>(WebRequest::Create(adres));
				 req->Credentials=gcnew NetworkCredential(username, password);
				 req->Method=WebRequestMethods::Ftp::UploadFile;
			 FileStream^ stru_plik= gcnew FileStream(openFileDialog1->FileName, FileMode::Open);
			 Stream^ req_stream= req->GetRequestStream();

			 int ile_bajtow;
				 array<Byte>^ bufor = gcnew array<Byte>(1024);
				 do {
					ile_bajtow=stru_plik->Read(bufor,0,1024);
					req_stream->Write(bufor,0,ile_bajtow);
				 }while (ile_bajtow!=0);
				
				 req_stream->Close();
				 
				 MessageBox::Show("Uploading File Completed", "Confirmation", MessageBoxButtons::OK,MessageBoxIcon::Information);
			 }
		 }
};
}

