 #include <cmath>
#pragma once

namespace Dudnienie {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::HScrollBar^  F1ScrollBar;
	protected: 

	private: System::Windows::Forms::HScrollBar^  W1ScrollBar;

	private: System::Windows::Forms::HScrollBar^  A1ScrollBar;
	private: System::Windows::Forms::TextBox^  YktextBox;


	private: System::Windows::Forms::TextBox^  YptextBox;

	private: System::Windows::Forms::TextBox^  XktextBox;

	private: System::Windows::Forms::TextBox^  XptextBox;

	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::CheckBox^  Sin3checkBox;

	private: System::Windows::Forms::CheckBox^  Sin2checkBox;

	private: System::Windows::Forms::CheckBox^  Sin1checkBox;

	private: System::Windows::Forms::TextBox^  F2textBox;

	private: System::Windows::Forms::TextBox^  W2textBox;

	private: System::Windows::Forms::TextBox^  A2textBox;

	private: System::Windows::Forms::TextBox^  F1textBox;

	private: System::Windows::Forms::TextBox^  W1textBox;

	private: System::Windows::Forms::TextBox^  A1textBox;

	private: System::Windows::Forms::HScrollBar^  F2ScrollBar;

	private: System::Windows::Forms::HScrollBar^  W2ScrollBar;

	private: System::Windows::Forms::HScrollBar^  A2ScrollBar;

	private: System::Windows::Forms::Panel^  panel2;
	private: System::Windows::Forms::GroupBox^  groupBox1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->Sin3checkBox = (gcnew System::Windows::Forms::CheckBox());
			this->Sin2checkBox = (gcnew System::Windows::Forms::CheckBox());
			this->Sin1checkBox = (gcnew System::Windows::Forms::CheckBox());
			this->F2textBox = (gcnew System::Windows::Forms::TextBox());
			this->W2textBox = (gcnew System::Windows::Forms::TextBox());
			this->A2textBox = (gcnew System::Windows::Forms::TextBox());
			this->F1textBox = (gcnew System::Windows::Forms::TextBox());
			this->W1textBox = (gcnew System::Windows::Forms::TextBox());
			this->A1textBox = (gcnew System::Windows::Forms::TextBox());
			this->F2ScrollBar = (gcnew System::Windows::Forms::HScrollBar());
			this->W2ScrollBar = (gcnew System::Windows::Forms::HScrollBar());
			this->A2ScrollBar = (gcnew System::Windows::Forms::HScrollBar());
			this->F1ScrollBar = (gcnew System::Windows::Forms::HScrollBar());
			this->W1ScrollBar = (gcnew System::Windows::Forms::HScrollBar());
			this->A1ScrollBar = (gcnew System::Windows::Forms::HScrollBar());
			this->YktextBox = (gcnew System::Windows::Forms::TextBox());
			this->YptextBox = (gcnew System::Windows::Forms::TextBox());
			this->XktextBox = (gcnew System::Windows::Forms::TextBox());
			this->XptextBox = (gcnew System::Windows::Forms::TextBox());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->panel1->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// panel1
			// 
			this->panel1->Controls->Add(this->groupBox1);
			this->panel1->Dock = System::Windows::Forms::DockStyle::Left;
			this->panel1->Location = System::Drawing::Point(0, 0);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(262, 449);
			this->panel1->TabIndex = 0;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label11);
			this->groupBox1->Controls->Add(this->label12);
			this->groupBox1->Controls->Add(this->label13);
			this->groupBox1->Controls->Add(this->label14);
			this->groupBox1->Controls->Add(this->label15);
			this->groupBox1->Controls->Add(this->label16);
			this->groupBox1->Controls->Add(this->label8);
			this->groupBox1->Controls->Add(this->label9);
			this->groupBox1->Controls->Add(this->label10);
			this->groupBox1->Controls->Add(this->label7);
			this->groupBox1->Controls->Add(this->label6);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->Sin3checkBox);
			this->groupBox1->Controls->Add(this->Sin2checkBox);
			this->groupBox1->Controls->Add(this->Sin1checkBox);
			this->groupBox1->Controls->Add(this->F2textBox);
			this->groupBox1->Controls->Add(this->W2textBox);
			this->groupBox1->Controls->Add(this->A2textBox);
			this->groupBox1->Controls->Add(this->F1textBox);
			this->groupBox1->Controls->Add(this->W1textBox);
			this->groupBox1->Controls->Add(this->A1textBox);
			this->groupBox1->Controls->Add(this->F2ScrollBar);
			this->groupBox1->Controls->Add(this->W2ScrollBar);
			this->groupBox1->Controls->Add(this->A2ScrollBar);
			this->groupBox1->Controls->Add(this->F1ScrollBar);
			this->groupBox1->Controls->Add(this->W1ScrollBar);
			this->groupBox1->Controls->Add(this->A1ScrollBar);
			this->groupBox1->Controls->Add(this->YktextBox);
			this->groupBox1->Controls->Add(this->YptextBox);
			this->groupBox1->Controls->Add(this->XktextBox);
			this->groupBox1->Controls->Add(this->XptextBox);
			this->groupBox1->Dock = System::Windows::Forms::DockStyle::Top;
			this->groupBox1->Location = System::Drawing::Point(0, 0);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(262, 445);
			this->groupBox1->TabIndex = 35;
			this->groupBox1->TabStop = false;
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(168, 271);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(25, 13);
			this->label11->TabIndex = 34;
			this->label11->Text = L"F2=";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(167, 245);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(30, 13);
			this->label12->TabIndex = 33;
			this->label12->Text = L"W2=";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(167, 219);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(26, 13);
			this->label13->TabIndex = 32;
			this->label13->Text = L"A2=";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(167, 192);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(25, 13);
			this->label14->TabIndex = 31;
			this->label14->Text = L"F1=";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(167, 165);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(30, 13);
			this->label15->TabIndex = 30;
			this->label15->Text = L"W1=";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(167, 138);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(26, 13);
			this->label16->TabIndex = 29;
			this->label16->Text = L"A1=";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(8, 268);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(19, 13);
			this->label8->TabIndex = 28;
			this->label8->Text = L"F2";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(7, 242);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(24, 13);
			this->label9->TabIndex = 27;
			this->label9->Text = L"W2";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(7, 216);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(20, 13);
			this->label10->TabIndex = 26;
			this->label10->Text = L"A2";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(7, 189);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(19, 13);
			this->label7->TabIndex = 25;
			this->label7->Text = L"F1";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(7, 162);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(24, 13);
			this->label6->TabIndex = 24;
			this->label6->Text = L"W1";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(7, 135);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(20, 13);
			this->label5->TabIndex = 23;
			this->label5->Text = L"A1";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(110, 62);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(32, 13);
			this->label4->TabIndex = 22;
			this->label4->Text = L"Y k =";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(110, 36);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(32, 13);
			this->label3->TabIndex = 21;
			this->label3->Text = L"X k =";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 62);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(32, 13);
			this->label2->TabIndex = 20;
			this->label2->Text = L"Y p =";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 36);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(32, 13);
			this->label1->TabIndex = 19;
			this->label1->Text = L"X p =";
			// 
			// Sin3checkBox
			// 
			this->Sin3checkBox->AutoSize = true;
			this->Sin3checkBox->Location = System::Drawing::Point(12, 401);
			this->Sin3checkBox->Name = L"Sin3checkBox";
			this->Sin3checkBox->Size = System::Drawing::Size(86, 17);
			this->Sin3checkBox->TabIndex = 18;
			this->Sin3checkBox->Text = L"Sin 1 + Sin 2";
			this->Sin3checkBox->UseVisualStyleBackColor = true;
			this->Sin3checkBox->CheckedChanged += gcnew System::EventHandler(this, &Form1::Sin3checkBox_CheckedChanged);
			// 
			// Sin2checkBox
			// 
			this->Sin2checkBox->AutoSize = true;
			this->Sin2checkBox->Location = System::Drawing::Point(12, 378);
			this->Sin2checkBox->Name = L"Sin2checkBox";
			this->Sin2checkBox->Size = System::Drawing::Size(50, 17);
			this->Sin2checkBox->TabIndex = 17;
			this->Sin2checkBox->Text = L"Sin 2";
			this->Sin2checkBox->UseVisualStyleBackColor = true;
			this->Sin2checkBox->CheckedChanged += gcnew System::EventHandler(this, &Form1::Sin2checkBox_CheckedChanged);
			// 
			// Sin1checkBox
			// 
			this->Sin1checkBox->AutoSize = true;
			this->Sin1checkBox->Location = System::Drawing::Point(12, 355);
			this->Sin1checkBox->Name = L"Sin1checkBox";
			this->Sin1checkBox->Size = System::Drawing::Size(50, 17);
			this->Sin1checkBox->TabIndex = 16;
			this->Sin1checkBox->Text = L"Sin 1";
			this->Sin1checkBox->UseVisualStyleBackColor = true;
			this->Sin1checkBox->CheckStateChanged += gcnew System::EventHandler(this, &Form1::Sin1checkBox_CheckStateChanged);
			// 
			// F2textBox
			// 
			this->F2textBox->Location = System::Drawing::Point(202, 264);
			this->F2textBox->Name = L"F2textBox";
			this->F2textBox->Size = System::Drawing::Size(44, 20);
			this->F2textBox->TabIndex = 15;
			// 
			// W2textBox
			// 
			this->W2textBox->Location = System::Drawing::Point(202, 238);
			this->W2textBox->Name = L"W2textBox";
			this->W2textBox->Size = System::Drawing::Size(44, 20);
			this->W2textBox->TabIndex = 14;
			// 
			// A2textBox
			// 
			this->A2textBox->Location = System::Drawing::Point(202, 212);
			this->A2textBox->Name = L"A2textBox";
			this->A2textBox->Size = System::Drawing::Size(44, 20);
			this->A2textBox->TabIndex = 13;
			// 
			// F1textBox
			// 
			this->F1textBox->Location = System::Drawing::Point(202, 185);
			this->F1textBox->Name = L"F1textBox";
			this->F1textBox->Size = System::Drawing::Size(44, 20);
			this->F1textBox->TabIndex = 12;
			// 
			// W1textBox
			// 
			this->W1textBox->Location = System::Drawing::Point(202, 157);
			this->W1textBox->Name = L"W1textBox";
			this->W1textBox->Size = System::Drawing::Size(44, 20);
			this->W1textBox->TabIndex = 11;
			// 
			// A1textBox
			// 
			this->A1textBox->Location = System::Drawing::Point(202, 131);
			this->A1textBox->Name = L"A1textBox";
			this->A1textBox->Size = System::Drawing::Size(44, 20);
			this->A1textBox->TabIndex = 10;
			// 
			// F2ScrollBar
			// 
			this->F2ScrollBar->LargeChange = 1;
			this->F2ScrollBar->Location = System::Drawing::Point(50, 264);
			this->F2ScrollBar->Maximum = 36000;
			this->F2ScrollBar->Name = L"F2ScrollBar";
			this->F2ScrollBar->Size = System::Drawing::Size(104, 17);
			this->F2ScrollBar->TabIndex = 9;
			this->F2ScrollBar->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &Form1::F2ScrollBar_Scroll);
			// 
			// W2ScrollBar
			// 
			this->W2ScrollBar->LargeChange = 1;
			this->W2ScrollBar->Location = System::Drawing::Point(50, 238);
			this->W2ScrollBar->Name = L"W2ScrollBar";
			this->W2ScrollBar->Size = System::Drawing::Size(104, 17);
			this->W2ScrollBar->TabIndex = 8;
			this->W2ScrollBar->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &Form1::W2ScrollBar_Scroll);
			// 
			// A2ScrollBar
			// 
			this->A2ScrollBar->LargeChange = 1;
			this->A2ScrollBar->Location = System::Drawing::Point(50, 212);
			this->A2ScrollBar->Maximum = 1000;
			this->A2ScrollBar->Name = L"A2ScrollBar";
			this->A2ScrollBar->Size = System::Drawing::Size(104, 17);
			this->A2ScrollBar->TabIndex = 7;
			this->A2ScrollBar->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &Form1::A2ScrollBar_Scroll);
			// 
			// F1ScrollBar
			// 
			this->F1ScrollBar->LargeChange = 1;
			this->F1ScrollBar->Location = System::Drawing::Point(50, 185);
			this->F1ScrollBar->Maximum = 36000;
			this->F1ScrollBar->Name = L"F1ScrollBar";
			this->F1ScrollBar->Size = System::Drawing::Size(104, 20);
			this->F1ScrollBar->TabIndex = 6;
			this->F1ScrollBar->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &Form1::F1ScrollBar_Scroll);
			// 
			// W1ScrollBar
			// 
			this->W1ScrollBar->LargeChange = 1;
			this->W1ScrollBar->Location = System::Drawing::Point(50, 158);
			this->W1ScrollBar->Name = L"W1ScrollBar";
			this->W1ScrollBar->Size = System::Drawing::Size(104, 20);
			this->W1ScrollBar->TabIndex = 5;
			this->W1ScrollBar->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &Form1::W1ScrollBar_Scroll);
			// 
			// A1ScrollBar
			// 
			this->A1ScrollBar->LargeChange = 1;
			this->A1ScrollBar->Location = System::Drawing::Point(50, 131);
			this->A1ScrollBar->Maximum = 1000;
			this->A1ScrollBar->Name = L"A1ScrollBar";
			this->A1ScrollBar->Size = System::Drawing::Size(104, 17);
			this->A1ScrollBar->TabIndex = 4;
			this->A1ScrollBar->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &Form1::A1ScrollBar_Scroll);
			// 
			// YktextBox
			// 
			this->YktextBox->Location = System::Drawing::Point(151, 55);
			this->YktextBox->Name = L"YktextBox";
			this->YktextBox->Size = System::Drawing::Size(55, 20);
			this->YktextBox->TabIndex = 3;
			this->YktextBox->Text = L"40";
			this->YktextBox->TextChanged += gcnew System::EventHandler(this, &Form1::YktextBox_TextChanged);
			// 
			// YptextBox
			// 
			this->YptextBox->Location = System::Drawing::Point(50, 55);
			this->YptextBox->Name = L"YptextBox";
			this->YptextBox->Size = System::Drawing::Size(53, 20);
			this->YptextBox->TabIndex = 2;
			this->YptextBox->Text = L"-40";
			this->YptextBox->TextChanged += gcnew System::EventHandler(this, &Form1::YptextBox_TextChanged);
			// 
			// XktextBox
			// 
			this->XktextBox->Location = System::Drawing::Point(151, 29);
			this->XktextBox->Name = L"XktextBox";
			this->XktextBox->Size = System::Drawing::Size(55, 20);
			this->XktextBox->TabIndex = 1;
			this->XktextBox->Text = L"40";
			this->XktextBox->TextChanged += gcnew System::EventHandler(this, &Form1::XktextBox_TextChanged);
			// 
			// XptextBox
			// 
			this->XptextBox->Location = System::Drawing::Point(50, 29);
			this->XptextBox->Name = L"XptextBox";
			this->XptextBox->Size = System::Drawing::Size(53, 20);
			this->XptextBox->TabIndex = 0;
			this->XptextBox->Text = L"-40";
			this->XptextBox->TextChanged += gcnew System::EventHandler(this, &Form1::XptextBox_TextChanged);
			// 
			// panel2
			// 
			this->panel2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->panel2->Location = System::Drawing::Point(262, 0);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(492, 449);
			this->panel2->TabIndex = 1;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(754, 449);
			this->Controls->Add(this->panel2);
			this->Controls->Add(this->panel1);
			this->MaximumSize = System::Drawing::Size(770, 487);
			this->MinimumSize = System::Drawing::Size(770, 487);
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Waves Summing Simulator";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->panel1->ResumeLayout(false);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);

		}

System::Double dblA1, dblW1, dblF1, dblA2, dblW2, dblF2;
static System::Double x, Xp,Xk,Yp,Yk;
Graphics^ g1;
static const System::Double M_PI = 3.141592;
public: array<System::Drawing::PointF>^ punkty;
public: array<System::Drawing::PointF>^ punkty2;
Pen^ pioro1;

System::Double ex(System::Double x)
	{
		return ((x-Xp)/(Xk-Xp))*this->panel2->Width;
	}


System::Double ey(System::Double y)
	{
		return ((Yk-y)/(Yk-Yp))*this->panel2->Height;
	}

System::Int32 exInt(System::Double x)
	{
		return ((x-Xp)/(Xk-Xp))*this->panel2->Width;
	}


System::Int32 eyInt(System::Double y)
	{
		return ((Yk-y)/(Yk-Yp))*this->panel2->Height;
	}

System::Void Rysuj(){

	Graphics^ g1 = panel2->CreateGraphics();
	Pen^ pioro1 = gcnew Pen(System::Drawing::Color::Blue);
	Pen^ pioro2 = gcnew Pen(System::Drawing::Color::Red);
	Pen^ pioro3 = gcnew Pen(System::Drawing::Color::Green);
	Pen^ blackPen = gcnew Pen(System::Drawing::Color::Black);

	static System::Double y;
	g1->Clear(Color::White);
	
	//Rysowanie krzyza;
	g1->DrawLine(blackPen,Convert::ToSingle(ex(Xp)),Convert::ToSingle(ey(0)),Convert::ToSingle(ex(Xk)),Convert::ToSingle(ey(0)) );
	g1->DrawLine(blackPen,Convert::ToSingle(ex(0)),Convert::ToSingle(ey(Yp)),Convert::ToSingle(ex(0)),Convert::ToSingle(ey(Yk)));
	
	
   if(Sin1checkBox->Checked==true)
{		
		x=Xp;

	    for (System::Int32 i=0; i<(this->panel2->Width); i++)
		{			

			y=dblA1*sin(dblW1*x+dblF1*(M_PI/180));  

			 
				if (i< (this->panel2->Width)/2) 
				{
					punkty[i].X=Convert::ToSingle(ex(x));
					punkty[i].Y=Convert::ToSingle(ey(y));
					x=x+0.1;
				}

				if (i>=((this->panel2->Width)/2) && i< (this->panel2->Width) )
				{
					punkty[i].X=Convert::ToSingle(ex(x));
					punkty[i].Y=Convert::ToSingle(ey(y));
					x=x+0.1;
				}		
		
		}

		g1->DrawLines(pioro1, punkty);

}

/////////////////////////////////////////////////////////////

	   if(Sin2checkBox->Checked==true)
{		
		x=Xp;

	    for (System::Int32 i=0; i<(this->panel2->Width); i++)
		{			
            
			y=dblA2*sin(dblW2*x+dblF2*(M_PI/180));  

			 
				if (i< (this->panel2->Width)/2) 
				{
					punkty2[i].X=Convert::ToSingle(ex(x));
					punkty2[i].Y=Convert::ToSingle(ey(y));
					x=x+0.1;
				}

				if (i>=((this->panel2->Width)/2) && i< (this->panel2->Width) )
				{
					punkty2[i].X=Convert::ToSingle(ex(x));
					punkty2[i].Y=Convert::ToSingle(ey(y));
					x=x+0.1;
				}		
		
		}

		g1->DrawLines(pioro2, punkty2);

}


		if(Sin3checkBox->Checked==true)
{		
		x=Xp;

	    for (System::Int32 i=0; i<(this->panel2->Width); i++)
		{			
            
			y= dblA1*sin(dblW1*x+dblF1*(M_PI/180)) + dblA2*sin(dblW2*x+dblF2*(M_PI/180));  

			 
				if (i< (this->panel2->Width)/2) 
				{
					punkty2[i].X=Convert::ToSingle(ex(x));
					punkty2[i].Y=Convert::ToSingle(ey(y));
					x=x+0.1;
				}

				if (i>=((this->panel2->Width)/2) && i< (this->panel2->Width) )
				{
					punkty2[i].X=Convert::ToSingle(ex(x));
					punkty2[i].Y=Convert::ToSingle(ey(y));
					x=x+0.1;
				}		
		
		}

		g1->DrawLines(pioro3, punkty2);

}

}



#pragma endregion
	private: System::Void A1ScrollBar_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e) {

				 dblA1=A1ScrollBar->Value;
				 dblA1=dblA1/100;
				 A1textBox->Text=dblA1.ToString();
				 Rysuj();


			 }

private: System::Void W1ScrollBar_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e) {

				 dblW1=W1ScrollBar->Value;
				 dblW1=dblW1/100;
				 W1textBox->Text=dblW1.ToString();
				 Rysuj();
		 }

private: System::Void F1ScrollBar_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e) {

				 dblF1=F1ScrollBar->Value;
				 dblF1=dblF1/100;
				 F1textBox->Text=dblF1.ToString();
				 Rysuj();
		 }

private: System::Void A2ScrollBar_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e) {

				 dblA2=A2ScrollBar->Value;
				 dblA2=dblA2/100;
				 A2textBox->Text=dblA2.ToString();
				 Rysuj();
		 }
private: System::Void W2ScrollBar_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e) {

				 dblW2=W2ScrollBar->Value;
				 dblW2=dblW2/100;
				 W2textBox->Text=dblW2.ToString();
				 Rysuj();
		 }

private: System::Void F2ScrollBar_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e) {

				 dblF2=F2ScrollBar->Value;
				 dblF2=dblF2/100;
				 F2textBox->Text=dblF2.ToString();
				 Rysuj();
		 }

private: System::Void XptextBox_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  try
			 {
				
				Xp=Convert::ToDouble(XptextBox->Text);
			 }
				
			 catch(...)

			 {

			 }
		 }



private: System::Void XktextBox_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  try
			 {
				
				Xk=System::Double::Parse(XktextBox->Text);
			 }
				
			 catch(...)

			 {

			 }
		 }


private: System::Void YptextBox_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  try
			 {
				
				Yp=System::Double::Parse(YptextBox->Text);
			 }
				
			 catch(...)

			 {

			 }
		 }



private: System::Void YktextBox_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  try
			 {
				
				Yk=System::Double::Parse(YktextBox->Text);
			 }
				
			 catch(...)

			 {

			 }
		 }
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {

			  g1 =this->panel2->CreateGraphics();
			  punkty = gcnew array<System::Drawing::PointF>(this->panel2->Width);
			  punkty2 = gcnew array<System::Drawing::PointF>(this->panel2->Width);

			  Xp=-10;
			  Xk=10;
			  Yp=-10;
			  Yk=10;
			  
			  for (System::Int32 i=0; i<(this->panel2->Width); i++)

					{
				
						punkty[i].X=Convert::ToSingle(0);
						punkty[i].Y=Convert::ToSingle(0);
						punkty2[i].X=Convert::ToSingle(0);
						punkty2[i].Y=Convert::ToSingle(0);
						
					};



		 }


private: System::Void Sin1checkBox_CheckStateChanged(System::Object^  sender, System::EventArgs^  e) {

			 Rysuj();

		 }
private: System::Void Sin2checkBox_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

			 Rysuj();
		 }
private: System::Void Sin3checkBox_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

			 Rysuj();
		 }
};

}

