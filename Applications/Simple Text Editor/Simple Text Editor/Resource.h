//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Simple Text Editor.rc
//

#define IDS_APP_TITLE			103

#define IDR_MAINFRAME			128
#define IDD_SIMPLETEXTEDITOR_DIALOG	102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define IDM_EXIT				105

//////Dodane///////////////////////
#define IDM_OPEN				129
#define IDM_SAVE				130
#define IDM_CUT				    131
#define IDM_COPY				132
#define IDM_PASTE				133
#define IDM_UNDO				134

//////Dodane///////////////////////

#define IDI_SIMPLETEXTEDITOR			107
#define IDI_SMALL				108
#define IDC_SIMPLETEXTEDITOR			109
#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1
#endif
// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NO_MFC					130
#define _APS_NEXT_RESOURCE_VALUE	135
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		110
#endif
#endif
