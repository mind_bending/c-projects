// Simple Text Editor.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
///DODANE///////////
#include "commdlg.h"
#include "fstream"
///DODANE///////////
#include "Simple Text Editor.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst; // current instance
static HWND hwndEdit;

///DODANE///////////
CHAR bufor[32000];
static OPENFILENAME ofn;
LPCTSTR filtr[4];
LPCTSTR dom_roz ="txt";
static char nazwa_pliku[3 * 1024];
int zb_wejscie;
int zb_wyjscie;
///DODANE///////////


TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_SIMPLETEXTEDITOR, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SIMPLETEXTEDITOR));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SIMPLETEXTEDITOR));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_SIMPLETEXTEDITOR);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	/////DODANE
	char znak;
	int c=0;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
//////////////OTWIERANIE//////////////////////////////////
		case IDM_OPEN:
			ZeroMemory (&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = hWnd;
			nazwa_pliku[0]='\0';
			ofn.lpstrFile = nazwa_pliku;
			ofn.nMaxFile = sizeof(nazwa_pliku);
			ofn.lpstrTitle = " Open File";
			filtr[0] ="Text"; 
			filtr[1] = "*.txt";
			filtr[2] = "All Files";
			filtr[3] = "*.*\0";
			ofn.lpstrFilter = filtr[0];
			
			ofn.Flags = OFN_FILEMUSTEXIST | OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY;

			if (GetOpenFileName(&ofn)){
				std::ifstream zb_wejscie(ofn.lpstrFile,std::ios::in);
				c=0;
				for (int k=0; k<32000;k++)
					bufor[k]=NULL;
				do
				{
					zb_wejscie.read(&znak, 1);
					if(znak=='\n')
					{
						znak='\r';
						bufor[c]=znak;
						c++;
						znak='\n';
						bufor[c] = znak;
						c++;
					}
					else
					{bufor[c] = znak; c++;}


				}

				while (!zb_wejscie.eof());
				zb_wejscie.close();

			if (!SendMessage(hwndEdit, WM_SETTEXT , 0 , (LPARAM) bufor))
				MessageBox(hwndEdit, "File too large.", "Error", MB_OK);

			}
			break;


//////////////ZAPIS//////////////////////////////////
		case IDM_SAVE:
			ZeroMemory (&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = hWnd;
			nazwa_pliku[0]='\0';
			ofn.lpstrFile = nazwa_pliku;
			ofn.nMaxFile = sizeof(nazwa_pliku);
			ofn.lpstrTitle = " Save File";
			filtr[0] ="Text"; 
			filtr[1] = "*.txt";
			filtr[2] = "All Files";
			filtr[3] = "*.*\0";
			ofn.lpstrFilter = filtr[0];
			ofn.lpstrDefExt=dom_roz;
			
			ofn.Flags = OFN_FILEMUSTEXIST | OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY;

			if (GetSaveFileName(&ofn)){

				SendMessage(hwndEdit, WM_GETTEXT, (WPARAM) 31000, (LPARAM) bufor);
				std::ofstream zb_wyjscie(ofn.lpstrFile);
				zb_wyjscie.write(bufor, strlen(bufor));
				zb_wyjscie.close();


			}
			break;

//////////////WYCI�CIE////////////////////////////////

		case IDM_CUT:
			SendMessage(hwndEdit, WM_CUT,0 ,0);
			break;

//////////////KOPIUJ//////////////////////////////////

		case IDM_COPY:
			SendMessage(hwndEdit, WM_COPY,0 ,0);
			break;

//////////////WKLEJ//////////////////////////////////

		case IDM_PASTE:
			SendMessage(hwndEdit, WM_PASTE,0 ,0);
			break;

//////////////SCHOWEK//////////////////////////////////

		case IDM_UNDO:
			if (SendMessage(hwndEdit, EM_CANUNDO,0 ,0))
				SendMessage(hwndEdit, WM_UNDO,0 ,0);
			else
				MessageBox(hwndEdit,"Undo buffer is empty", "Undo", MB_OK);

			break;




		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_CREATE:
		hwndEdit = CreateWindow("EDIT", NULL, WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_MULTILINE | ES_WANTRETURN , 0 , 0 , 0 , 0 , hWnd,(HMENU)1,hInst,NULL);
		break;

	case WM_SIZE:
		MoveWindow(hwndEdit,0, 40, LOWORD(lParam), HIWORD(lParam)-40, TRUE);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
