#pragma once

namespace TowerofHanoi {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected: 
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  startToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  optionsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem2;
	private: System::Windows::Forms::ToolStripMenuItem^  disksToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  disksToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::Panel^  panel2;
	private: System::Windows::Forms::Panel^  panel3;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->startToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->optionsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->disksToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->disksToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->fileToolStripMenuItem, 
				this->optionsToolStripMenuItem, this->aboutToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(460, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->startToolStripMenuItem, 
				this->exitToolStripMenuItem});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// startToolStripMenuItem
			// 
			this->startToolStripMenuItem->Enabled = false;
			this->startToolStripMenuItem->Name = L"startToolStripMenuItem";
			this->startToolStripMenuItem->Size = System::Drawing::Size(98, 22);
			this->startToolStripMenuItem->Text = L"Start";
			this->startToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::startToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(98, 22);
			this->exitToolStripMenuItem->Text = L"Exit";
			// 
			// optionsToolStripMenuItem
			// 
			this->optionsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->toolStripMenuItem2, 
				this->disksToolStripMenuItem, this->disksToolStripMenuItem1});
			this->optionsToolStripMenuItem->Name = L"optionsToolStripMenuItem";
			this->optionsToolStripMenuItem->Size = System::Drawing::Size(61, 20);
			this->optionsToolStripMenuItem->Text = L"Options";
			// 
			// toolStripMenuItem2
			// 
			this->toolStripMenuItem2->CheckOnClick = true;
			this->toolStripMenuItem2->Name = L"toolStripMenuItem2";
			this->toolStripMenuItem2->Size = System::Drawing::Size(109, 22);
			this->toolStripMenuItem2->Text = L"3 disks";
			this->toolStripMenuItem2->Click += gcnew System::EventHandler(this, &Form1::toolStripMenuItem2_Click);
			// 
			// disksToolStripMenuItem
			// 
			this->disksToolStripMenuItem->CheckOnClick = true;
			this->disksToolStripMenuItem->Name = L"disksToolStripMenuItem";
			this->disksToolStripMenuItem->Size = System::Drawing::Size(109, 22);
			this->disksToolStripMenuItem->Text = L"5 disks";
			this->disksToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::disksToolStripMenuItem_Click);
			// 
			// disksToolStripMenuItem1
			// 
			this->disksToolStripMenuItem1->CheckOnClick = true;
			this->disksToolStripMenuItem1->Name = L"disksToolStripMenuItem1";
			this->disksToolStripMenuItem1->Size = System::Drawing::Size(109, 22);
			this->disksToolStripMenuItem1->Text = L"7 disks";
			this->disksToolStripMenuItem1->Click += gcnew System::EventHandler(this, &Form1::disksToolStripMenuItem1_Click);
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(52, 20);
			this->aboutToolStripMenuItem->Text = L"About";
			this->aboutToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::aboutToolStripMenuItem_Click);
			// 
			// panel1
			// 
			this->panel1->Location = System::Drawing::Point(65, 86);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(101, 290);
			this->panel1->TabIndex = 1;
			this->panel1->Click += gcnew System::EventHandler(this, &Form1::panel1_Click);
			// 
			// panel2
			// 
			this->panel2->Location = System::Drawing::Point(184, 86);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(101, 290);
			this->panel2->TabIndex = 2;
			this->panel2->Click += gcnew System::EventHandler(this, &Form1::panel2_Click);
			this->panel2->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Form1::panel2_Paint);
			// 
			// panel3
			// 
			this->panel3->Location = System::Drawing::Point(301, 86);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(101, 290);
			this->panel3->TabIndex = 3;
			this->panel3->Click += gcnew System::EventHandler(this, &Form1::panel3_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(460, 394);
			this->Controls->Add(this->panel3);
			this->Controls->Add(this->panel2);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"The Tower of Hanoi";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Form1::Form1_Paint);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

private: array<int,2>^ patyk;
private: array<int>^ max_k;
private: int zrodlo;
private: int cel;
private: int kod;
private: int ile_krazkow;
private: SolidBrush^ pedzel;
private: System::Void wyswietl_krazki(){
		 
			 Image^ bmp = Image::FromFile("krazek.jpg");
			 Image^ bmp_wyb = Image::FromFile("krazek_wyb.jpg");

			 Graphics^ g=panel1->CreateGraphics();
			 Graphics^ g1=panel2->CreateGraphics();
			 Graphics^ g2=panel3->CreateGraphics();

				 panel1->Refresh();
				 panel2->Refresh();
				 panel3->Refresh();

				 pedzel->Color=System::Drawing::Color::IndianRed;
				 g->FillRectangle(pedzel,48,0,5,280);
				 g1->FillRectangle(pedzel,48,0,5,280);
				 g2->FillRectangle(pedzel,48,0,5,280);

				 for (int i=1; i<max_k[0]+1;i++) {
					 if (patyk[0,i]>0) g->DrawImage(bmp, (panel1->Width/2)-(panel1->Width/((ile_krazkow-patyk[0,i])+1))/2,panel1->Height-i*20,panel1->Width/((ile_krazkow-patyk[0,i])+1),20);
					 }

				 if ((kod==1)&&(zrodlo==0))
				 {
				 g->DrawImage(bmp_wyb, (panel1->Width/2)-
				 (panel1->Width/((ile_krazkow-patyk[0,max_k[0]])+1))/2,panel1->Height-
				 max_k[0]*20,panel1->Width/((ile_krazkow-patyk[0,max_k[0]])+1),20);
				 }
				 
				 ///////////////// 5 k ///////////////

				 for (int i=1; i<max_k[1]+1;i++) {
					 if (patyk[1,i]>0) g1->DrawImage(bmp, (panel2->Width/2)-(panel2->Width/((ile_krazkow-
						 patyk[1,i])+1))/2,panel2->Height-i*20,panel2->Width/((ile_krazkow-patyk[1,i])+1),20);
					 }

				 if ((kod==1)&&(zrodlo==1))
				 {
				 g1->DrawImage(bmp_wyb, (panel2->Width/2)-
				 (panel2->Width/((ile_krazkow-patyk[1,max_k[1]])+1))/2,panel2->Height-
				 max_k[1]*20,panel2->Width/((ile_krazkow-patyk[1,max_k[1]])+1),20);
				 }



				 ///////////////// 7 k ///////////////

				 for (int i=1; i<max_k[2]+1;i++) {
					 if (patyk[2,i]>0) g2->DrawImage(bmp, (panel3->Width/2)-(panel3->Width/((ile_krazkow-
						 patyk[2,i])+1))/2,panel3->Height-i*20,panel3->Width/((ile_krazkow-patyk[2,i])+1),20);
					 }

				 if ((kod==1)&&(zrodlo==2))
				 {
				 g2->DrawImage(bmp_wyb, (panel3->Width/2)-
				 (panel3->Width/((ile_krazkow-patyk[2,max_k[2]])+1))/2,panel3->Height-
				 max_k[2]*20,panel3->Width/((ile_krazkow-patyk[1,max_k[2]])+1),20);
				 }


		 };


	private: System::Void przesun_krazki(){
			 
				 if(max_k[zrodlo]>0) {
					 if ((patyk[zrodlo,max_k[zrodlo]]<patyk[cel, max_k[cel]])||(max_k[cel]==0))
					 {
						 
						 max_k[cel]++;
						 patyk[cel, max_k[cel]]=patyk[zrodlo,max_k[zrodlo]];
						 max_k[zrodlo]--;
					 }
					 else
						 MessageBox::Show("You can't put bigger disk on smaller one", "Warning",
						 MessageBoxButtons::OK, MessageBoxIcon::Error);
			 }
				 else MessageBox::Show("There is no disks to move", "Error",
					 MessageBoxButtons::OK, MessageBoxIcon::Error);
		 };


	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {

				 patyk=gcnew array<int,2>(3,20);
				 max_k=gcnew array<int>(3);
				 pedzel=gcnew SolidBrush(System::Drawing::Color::IndianRed);

			 }


private: System::Void toolStripMenuItem2_Click(System::Object^  sender, System::EventArgs^  e) {

			 startToolStripMenuItem->Enabled=true;

			 toolStripMenuItem2->Checked=true;
			 disksToolStripMenuItem->Checked=false;
			 disksToolStripMenuItem1->Checked=false;
			 ile_krazkow=3;

		 }
private: System::Void disksToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			 startToolStripMenuItem->Enabled=true;

			 toolStripMenuItem2->Checked=false;
			 disksToolStripMenuItem->Checked=true;
			 disksToolStripMenuItem1->Checked=false;
			 ile_krazkow=5;
		 }
private: System::Void disksToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {

			 startToolStripMenuItem->Enabled=true;

			 toolStripMenuItem2->Checked=false;
			 disksToolStripMenuItem->Checked=false;
			 disksToolStripMenuItem1->Checked=true;
			 ile_krazkow=7;
		 }
private: System::Void startToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			 max_k[0]=ile_krazkow;
			 max_k[1]=0;
			 max_k[2]=0;

			 kod=0;
			 for (int k=1; k<max_k[0]+1;k++)
				patyk[0,k]=max_k[0]-(k-1);

			wyswietl_krazki();
		 }
private: System::Void panel1_Click(System::Object^  sender, System::EventArgs^  e) {

			 if (kod==0)
			 {
				 zrodlo=0; kod=1;
				 wyswietl_krazki();
				 return;
			 }


			 if (kod==1)
			 {
				 cel=0;
				 kod=0;
				 przesun_krazki();
				 wyswietl_krazki();
			 }
		 }
private: System::Void panel2_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
		 }


private: System::Void panel2_Click(System::Object^  sender, System::EventArgs^  e) {

			 if (kod==0)
			 {
				 zrodlo=1; kod=1;
				 wyswietl_krazki();
				 return;
			 }


			 if (kod==1)
			 {
				 cel=1;
				 kod=0;
				 przesun_krazki();
				 wyswietl_krazki();
			 }
		 }
private: System::Void panel3_Click(System::Object^  sender, System::EventArgs^  e) {

			 if (kod==0)
			 {
				 zrodlo=2; kod=1;
				 wyswietl_krazki();
				 return;
			 }


			 if (kod==1)
			 {
				 cel=2;
				 kod=0;
				 przesun_krazki();
				 wyswietl_krazki();
			 }
		 }
private: System::Void Form1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
			 wyswietl_krazki();
		 }
private: System::Void aboutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 MessageBox::Show("Artur Wieczorek", "The Hanoi Towers",
					 MessageBoxButtons::OK, MessageBoxIcon::Information);
		 }
};
}

