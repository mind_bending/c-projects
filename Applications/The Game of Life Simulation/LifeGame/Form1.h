#pragma once

namespace LifeGame {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected: 
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  typeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  theGameOfToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  seedsToolStripMenuItem;
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  pattern1ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  pattern2ToolStripMenuItem;
	private: System::Windows::Forms::Button^  button4;

	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->pattern1ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->pattern2ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->typeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->theGameOfToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->seedsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView1))->BeginInit();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->fileToolStripMenuItem, 
				this->typeToolStripMenuItem, this->aboutToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(546, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->exitToolStripMenuItem, 
				this->pattern1ToolStripMenuItem, this->pattern2ToolStripMenuItem});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			this->fileToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::fileToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::exitToolStripMenuItem_Click);
			// 
			// pattern1ToolStripMenuItem
			// 
			this->pattern1ToolStripMenuItem->CheckOnClick = true;
			this->pattern1ToolStripMenuItem->Name = L"pattern1ToolStripMenuItem";
			this->pattern1ToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->pattern1ToolStripMenuItem->Text = L"Pattern 1";
			this->pattern1ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::pattern1ToolStripMenuItem_Click);
			// 
			// pattern2ToolStripMenuItem
			// 
			this->pattern2ToolStripMenuItem->CheckOnClick = true;
			this->pattern2ToolStripMenuItem->Name = L"pattern2ToolStripMenuItem";
			this->pattern2ToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->pattern2ToolStripMenuItem->Text = L"Pattern 2";
			this->pattern2ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::pattern2ToolStripMenuItem_Click);
			// 
			// typeToolStripMenuItem
			// 
			this->typeToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->theGameOfToolStripMenuItem, 
				this->seedsToolStripMenuItem});
			this->typeToolStripMenuItem->Name = L"typeToolStripMenuItem";
			this->typeToolStripMenuItem->Size = System::Drawing::Size(45, 20);
			this->typeToolStripMenuItem->Text = L"Type";
			// 
			// theGameOfToolStripMenuItem
			// 
			this->theGameOfToolStripMenuItem->CheckOnClick = true;
			this->theGameOfToolStripMenuItem->Name = L"theGameOfToolStripMenuItem";
			this->theGameOfToolStripMenuItem->Size = System::Drawing::Size(164, 22);
			this->theGameOfToolStripMenuItem->Text = L"The Game of Life";
			this->theGameOfToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::theGameOfToolStripMenuItem_Click);
			// 
			// seedsToolStripMenuItem
			// 
			this->seedsToolStripMenuItem->CheckOnClick = true;
			this->seedsToolStripMenuItem->Name = L"seedsToolStripMenuItem";
			this->seedsToolStripMenuItem->Size = System::Drawing::Size(164, 22);
			this->seedsToolStripMenuItem->Text = L"Seeds";
			this->seedsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::seedsToolStripMenuItem_Click);
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(52, 20);
			this->aboutToolStripMenuItem->Text = L"About";
			this->aboutToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::aboutToolStripMenuItem_Click);
			// 
			// dataGridView1
			// 
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Location = System::Drawing::Point(12, 36);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->RowTemplate->ReadOnly = true;
			this->dataGridView1->Size = System::Drawing::Size(522, 443);
			this->dataGridView1->TabIndex = 1;
			this->dataGridView1->CellClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Form1::dataGridView1_CellClick);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 500);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 2;
			this->button1->Text = L"Step";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(93, 500);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 3;
			this->button2->Text = L"Auto";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(174, 500);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 23);
			this->button3->TabIndex = 4;
			this->button3->Text = L"Stop";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
			// 
			// timer1
			// 
			this->timer1->Interval = 1000;
			this->timer1->Tick += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(255, 500);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(75, 23);
			this->button4->TabIndex = 5;
			this->button4->Text = L"Clear All";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(546, 535);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->dataGridView1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"The Game of Life";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void theGameOfToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				 if( seedsToolStripMenuItem->Checked == true){
					 seedsToolStripMenuItem->Checked = false;
					 theGameOfToolStripMenuItem->Checked = true;
				 }else{
					 theGameOfToolStripMenuItem->Checked = true;
				 }
			 }
	private: System::Void seedsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				 if(theGameOfToolStripMenuItem->Checked == true){
					 theGameOfToolStripMenuItem->Checked = false;
					 seedsToolStripMenuItem->Checked = true;
				 }
				 seedsToolStripMenuItem->Checked = true;
			 }

	private: array<int,2>^ plansza;

	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {

				 dataGridView1->ColumnCount=20;
				 dataGridView1->RowCount=20;
				 dataGridView1->AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode::Fill;
				 dataGridView1->RowHeadersVisible=false;
				 dataGridView1->ColumnHeadersVisible=false;
				 dataGridView1->AllowUserToResizeRows=false;
				 dataGridView1->AllowUserToResizeColumns=false;

				 plansza = gcnew array<int,2>(31,31);
				 clearBoard();

			 }

	private: System::Void dataGridView1_CellClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {

				 if
					 (dataGridView1->Rows[dataGridView1->CurrentCell->RowIndex]->Cells[dataGridView1->
					 CurrentCell->ColumnIndex]->Style->BackColor!=System::Drawing::Color::Green)
				 {
					 dataGridView1->Rows[dataGridView1->CurrentCell->RowIndex]->Cells[dataGridView1->
						 CurrentCell->ColumnIndex]->Style->BackColor=System::Drawing::Color::Green;
				 }

				 else
					 dataGridView1->Rows[dataGridView1->CurrentCell->RowIndex]->Cells[dataGridView1->
					 CurrentCell->ColumnIndex]->Style->BackColor=System::Drawing::Color::White;

			 }


	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {


				 if(!isBoardEmpty() && !pattern1ToolStripMenuItem->Checked && !pattern2ToolStripMenuItem->Checked){
					 MessageBox::Show("Draw some fields first, then select generator type from 'Type' menu" , "Information", MessageBoxButtons::OK, MessageBoxIcon::Information); 
				 }
				 else if (theGameOfToolStripMenuItem->Checked==false && seedsToolStripMenuItem->Checked==false){
					 MessageBox::Show("Please, select the type first from 'Type' menu" , "Information", MessageBoxButtons::OK, MessageBoxIcon::Information); 
				 }
				 else if (theGameOfToolStripMenuItem->Checked==true)
				 {
					 int sasiad=0;

					 for (int j=0; j<20; j++) {

						 for (int i=0; i<20;i++) {
							 for (int j1=j-1; j1<j+2; j1++) {
								 for (int i1=i-1; i1<i+2; i1++) {
									 if ((j1>=0)&&(j1<20)&&(i1>=0)&&(i1<20))
									 {if (dataGridView1->Rows[i1]->Cells[j1]->Style->BackColor==System::Drawing::Color::Green)
									 if(!((i1==i)&&(j1==j))) sasiad++;}
								 }
							 }




							 plansza[i,j]=0;

							 if (sasiad==3) 
							 {
								 plansza[i,j]=1;
							 }

							 if ((sasiad==2)&&(dataGridView1->Rows[i]->Cells[j]->Style->BackColor==System::Drawing::Color::Green))
							 {
								 plansza[i,j]=1;
							 }

							 sasiad=0;
						 }

					 }

					 for (int j=0; j<20; j++) {
						 for (int i=0; i<20; i++) {
							 if (plansza[i,j]==1)
								 dataGridView1->Rows[i]->Cells[j]->Style->BackColor=System::Drawing::Color::Green;
							 else
								 dataGridView1->Rows[i]->Cells[j]->Style->BackColor=System::Drawing::Color::White;
						 }
					 }

					 if(!isBoardEmpty()){
						 timer1->Enabled = false;
						 pattern1ToolStripMenuItem->Checked=false;
						 pattern2ToolStripMenuItem->Checked=false;
						 theGameOfToolStripMenuItem->Checked=false;
						 seedsToolStripMenuItem->Checked=false;
						 button2->Enabled=true;
					 }

				 }

				 //Tryb seeds
				 else if (seedsToolStripMenuItem->Checked==true){

					 int sasiad=0;

					 for (int j=0; j<20; j++) {

						 for (int i=0; i<20;i++) {
							 for (int j1=j-1; j1<j+2; j1++) {
								 for (int i1=i-1; i1<i+2; i1++) {
									 if ((j1>=0)&&(j1<20)&&(i1>=0)&&(i1<20))
									 {if (dataGridView1->Rows[i1]->Cells[j1]->Style->BackColor==System::Drawing::Color::Green)
									 if(!((i1==i)&&(j1==j))) sasiad++;}
								 }
							 }

							 plansza[i,j]=0;

							 if (sasiad==2) plansza [i,j]=1;
							 sasiad=0;
						 }
					 }
					 for (int j=0; j<20; j++) {
						 for (int i=0; i<20; i++) {
							 if (plansza[i,j]==1)
								 dataGridView1->Rows[i]->Cells[j]->Style->BackColor=System::Drawing::Color::Green;
							 else
								 dataGridView1->Rows[i]->Cells[j]->Style->BackColor=System::Drawing::Color::White;
						 }
					 }

					 if(!isBoardEmpty()){
						 timer1->Enabled = false;
						 pattern1ToolStripMenuItem->Checked=false;
						 pattern2ToolStripMenuItem->Checked=false;
						 theGameOfToolStripMenuItem->Checked=false;
						 seedsToolStripMenuItem->Checked=false;
						 button2->Enabled=true;
					 }
				 }





			 };

	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {

				 if(!isBoardEmpty()){
					 MessageBox::Show("Draw some fields first, then select generator type from 'Type' menu" , "Information", MessageBoxButtons::OK, MessageBoxIcon::Information); 
				 }

				 else if (theGameOfToolStripMenuItem->Checked==false && seedsToolStripMenuItem->Checked==false){
					 MessageBox::Show("Please, select the type first from 'Type' menu" , "Information", MessageBoxButtons::OK, MessageBoxIcon::Information); 
				 }
				 else{
					 button2->Enabled=false;
					 timer1->Enabled=true;
					 button3->Enabled=true;

				 }

				 if(!isBoardEmpty()){
					 timer1->Enabled = false;
					 pattern1ToolStripMenuItem->Checked=false;
					 pattern2ToolStripMenuItem->Checked=false;
					 theGameOfToolStripMenuItem->Checked=false;
					 seedsToolStripMenuItem->Checked=false;
				 }
			 }

	private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {

				 button2->Enabled=true;
				 timer1->Enabled=false;
				 button3->Enabled=false;
			 }

	private: System::Void aboutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				 MessageBox::Show("Author: Artur Wieczorek\nSelect some fields (or predefined pattern), choose generator Type\nand use Step or Auto to start simulation." , "The Game of Life", MessageBoxButtons::OK, MessageBoxIcon::Information); 
			 }

	private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 Close();
			 }

	private: System::Void pattern1ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				 if(pattern1ToolStripMenuItem->Checked){


					 timer1->Enabled=false;
					 pattern2ToolStripMenuItem->Checked=false;
					 button2->Enabled=true;
					 button3->Enabled=true;

					 //czyszczenie tablicy
					 for (int j=0; j<20; j++) 
					 {
						 for (int i=0; i<20; i++) 
						 {
							 dataGridView1->Rows[j]->Cells[i]->Style->BackColor=System::Drawing::Color::White;
						 }
					 }

					 //koniec czyszczenia tablicy
					 dataGridView1->Rows[10]->Cells[10]->Style->BackColor=System::Drawing::Color::Green;
					 dataGridView1->Rows[10]->Cells[11]->Style->BackColor=System::Drawing::Color::Green;

				 }
	
				 else 
				 {
					 timer1->Enabled=false;
				 }
			 }


	private: System::Void pattern2ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				 if(pattern2ToolStripMenuItem->Checked){
					 timer1->Enabled=false;
					 pattern1ToolStripMenuItem->Checked=false;
					 button2->Enabled=true;
					 button3->Enabled=true;

					 //czyszczenie tablicy
					 for (int j=0; j<20; j++) 
					 {
						 for (int i=0; i<20; i++) 
						 {
							 dataGridView1->Rows[j]->Cells[i]->Style->BackColor=System::Drawing::Color::White;
						 }
					 }
					 //koniec czyszczenia tablicy

					 dataGridView1->Rows[10]->Cells[10]->Style->BackColor=System::Drawing::Color::Green;
					 dataGridView1->Rows[10]->Cells[12]->Style->BackColor=System::Drawing::Color::Green;

				 }


				 else 
				 {
					 timer1->Enabled=false;
				 }

			 }
	private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {

				 //Jezeli timer byl ustawiony to go wylacza
				 if(timer1->Enabled == true){
					 timer1->Enabled = false;
					 button3->Enabled = false;
					 button2->Enabled = true;
				 }


				 //czyszczenie tablicy
				 for (int j=0; j<20; j++) 
				 {
					 for (int i=0; i<20; i++) 
					 {
						 dataGridView1->Rows[j]->Cells[i]->Style->BackColor=System::Drawing::Color::White;
					 }
				 }
				 //koniec czyszczenia tablicy

				 //Odchaczanie guzikow
				 pattern1ToolStripMenuItem->Checked=false;
				 pattern2ToolStripMenuItem->Checked=false;
				 theGameOfToolStripMenuItem->Checked=false;
				 seedsToolStripMenuItem->Checked=false;

			 }
	private: System::Void fileToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 }

			 Boolean isBoardEmpty(){
				 for (int j=0; j<20; j++) 
				 {
					 for (int i=0; i<20; i++) 
					 {
						 if(dataGridView1->Rows[j]->Cells[i]->Style->BackColor!=System::Drawing::Color::White){
							 return true;
							 break;
						 }
					 }
				 }
				 return false;
			 }


			 Void clearBoard(){
				 for (int j=0; j<20; j++) 
				 {
					 for (int i=0; i<20; i++) 
					 {
						 dataGridView1->Rows[j]->Cells[i]->Style->BackColor==System::Drawing::Color::White;
					 }
				 }
			 }
	};
}

