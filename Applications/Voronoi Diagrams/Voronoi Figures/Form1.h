#pragma once

namespace VoronoiFigures {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected: 
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::ProgressBar^  progressBar1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Button^  button2;



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(569, 67);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(91, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Number of Points:";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(572, 92);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(100, 20);
			this->textBox1->TabIndex = 1;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(572, 159);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(100, 23);
			this->button1->TabIndex = 2;
			this->button1->Text = L"Draw!";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// progressBar1
			// 
			this->progressBar1->Location = System::Drawing::Point(572, 212);
			this->progressBar1->Name = L"progressBar1";
			this->progressBar1->Size = System::Drawing::Size(100, 23);
			this->progressBar1->TabIndex = 3;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(596, 253);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(0, 13);
			this->label2->TabIndex = 4;
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(572, 307);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(100, 23);
			this->button2->TabIndex = 5;
			this->button2->Text = L"Info";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(684, 662);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->progressBar1);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label1);
			this->Name = L"Form1";
			this->Text = L"Voronoi Shapes";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: System::Single odleglosc(System::Int32 x1, System::Int32 y1, System::Int32 x2, System::Int32 y2)
			 {
			 
				 return System::Math::Sqrt(((x1-x2)*(x1-x2))+((y1-y2)*(y1-y2)));
			 
			 }


	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

				 if (System::Int16::Parse(textBox1->Text)>11)
				 {
					 MessageBox::Show("11 is the maximum number of points you can enter" , "Warning", MessageBoxButtons::OK, MessageBoxIcon::Exclamation); 
					 return;
				 }

				array<System::Drawing::Color>^ kolory = gcnew array<System::Drawing::Color>(12);
				kolory[0]=System::Drawing::Color::Orange;
				kolory[1]=System::Drawing::Color::AliceBlue;
				kolory[2]=System::Drawing::Color::Red;
				kolory[3]=System::Drawing::Color::BlanchedAlmond;
				kolory[4]=System::Drawing::Color::Black;
				kolory[5]=System::Drawing::Color::DeepPink;
				kolory[6]=System::Drawing::Color::Green;
				kolory[7]=System::Drawing::Color::Orange;
				kolory[8]=System::Drawing::Color::Yellow;
				kolory[9]=System::Drawing::Color::Chocolate;
				kolory[10]=System::Drawing::Color::DarkMagenta;
				kolory[11]=System::Drawing::Color::Chartreuse;

				Graphics^ g1 = this->CreateGraphics();

				array<System::Int32>^ wspx = gcnew array<System::Int32>(11);
				array<System::Int32>^ wspy = gcnew array<System::Int32>(11);

				Bitmap^ bitmapa = gcnew Bitmap(550, 650);
				Random^ liczba_los = gcnew Random();

				for (System::Int32 i=0; i<System::Int16::Parse(textBox1->Text);i++)
				{
					wspx[i] = liczba_los->Next(bitmapa->Width);
					wspy[i] = liczba_los->Next(bitmapa->Height);
				}

				System::Int32 NajPunkt;
				System::Single odl;


				for (System::Int32 x=0; x<bitmapa->Width;x++)
					for (System::Int32 y=0; y<bitmapa->Height;y++)
					{
					
						{
							odl=30000;
							for (System::Int32 i=0; i<System::Int16::Parse(textBox1->Text);i++)
							{
								if (odleglosc(x,y,wspx[i],wspy[i])<odl)
								{
									NajPunkt=i;
									odl=odleglosc(x,y,wspx[i],wspy[i]);
								}

							}

						bitmapa->SetPixel(x,y,kolory[NajPunkt]);
						progressBar1->Value=(x+1)*((System::Single)1/(System::Single)bitmapa->Width)*100;
						

						if (progressBar1->Value==100)
							label2->Text="Completed!";
					}
			 }


					g1->DrawImage(dynamic_cast<Image^>(bitmapa),10,10);

	};
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {

			  
					 MessageBox::Show("Author: Artur Wieczorek" , "Voronoi Shapes", MessageBoxButtons::OK, MessageBoxIcon::Information); 
					 
				 
		 }
};

}

