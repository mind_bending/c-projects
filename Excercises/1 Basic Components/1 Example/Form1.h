#pragma once

namespace My1Example {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  btnExit;
	private: System::Windows::Forms::Button^  btnShowTxt;
	private: System::Windows::Forms::Label^  lblText;
	private: System::Windows::Forms::LinkLabel^  linkLabel1;
	private: System::Windows::Forms::Button^  btnWrite;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  textBox3;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->btnExit = (gcnew System::Windows::Forms::Button());
			this->btnShowTxt = (gcnew System::Windows::Forms::Button());
			this->lblText = (gcnew System::Windows::Forms::Label());
			this->linkLabel1 = (gcnew System::Windows::Forms::LinkLabel());
			this->btnWrite = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// btnExit
			// 
			this->btnExit->Location = System::Drawing::Point(220, 245);
			this->btnExit->Name = L"btnExit";
			this->btnExit->Size = System::Drawing::Size(66, 25);
			this->btnExit->TabIndex = 0;
			this->btnExit->Text = L"Exit";
			this->btnExit->UseVisualStyleBackColor = true;
			this->btnExit->Click += gcnew System::EventHandler(this, &Form1::btnExit_Click);
			// 
			// btnShowTxt
			// 
			this->btnShowTxt->Location = System::Drawing::Point(9, 90);
			this->btnShowTxt->Name = L"btnShowTxt";
			this->btnShowTxt->Size = System::Drawing::Size(80, 23);
			this->btnShowTxt->TabIndex = 1;
			this->btnShowTxt->Text = L"Show Label";
			this->btnShowTxt->UseVisualStyleBackColor = true;
			this->btnShowTxt->Click += gcnew System::EventHandler(this, &Form1::btnShowTxt_Click);
			// 
			// lblText
			// 
			this->lblText->AutoSize = true;
			this->lblText->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->lblText->Location = System::Drawing::Point(120, 162);
			this->lblText->Name = L"lblText";
			this->lblText->Size = System::Drawing::Size(0, 24);
			this->lblText->TabIndex = 2;
			// 
			// linkLabel1
			// 
			this->linkLabel1->AutoSize = true;
			this->linkLabel1->Location = System::Drawing::Point(12, 251);
			this->linkLabel1->Name = L"linkLabel1";
			this->linkLabel1->Size = System::Drawing::Size(108, 13);
			this->linkLabel1->TabIndex = 3;
			this->linkLabel1->TabStop = true;
			this->linkLabel1->Text = L"http://www.google.pl";
			this->linkLabel1->LinkClicked += gcnew System::Windows::Forms::LinkLabelLinkClickedEventHandler(this, &Form1::linkLabel1_LinkClicked);
			// 
			// btnWrite
			// 
			this->btnWrite->Location = System::Drawing::Point(12, 61);
			this->btnWrite->Name = L"btnWrite";
			this->btnWrite->Size = System::Drawing::Size(66, 23);
			this->btnWrite->TabIndex = 4;
			this->btnWrite->Text = L"Write Text";
			this->btnWrite->UseVisualStyleBackColor = true;
			this->btnWrite->Click += gcnew System::EventHandler(this, &Form1::btnWrite_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(9, 10);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(277, 45);
			this->textBox1->TabIndex = 5;
			this->textBox1->Text = L"This is TextBox Component";
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(88, 60);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(61, 20);
			this->textBox2->TabIndex = 6;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(163, 62);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(0, 13);
			this->label1->TabIndex = 7;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(9, 165);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(63, 20);
			this->textBox3->TabIndex = 8;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(6, 200);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(0, 13);
			this->label2->TabIndex = 10;
			// 
			// label3
			// 
			this->label3->Location = System::Drawing::Point(6, 128);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(280, 35);
			this->label3->TabIndex = 11;
			this->label3->Text = L"Enter number of the line from TextBox you want to display and click OK. Index starts from 0. (0 = First Line)";
			this->label3->Click += gcnew System::EventHandler(this, &Form1::label3_Click);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(78, 162);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(30, 23);
			this->button1->TabIndex = 12;
			this->button1->Text = L"OK";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(228, 90);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(58, 23);
			this->button2->TabIndex = 13;
			this->button2->Text = L"Clear Text";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::ControlLight;
			this->ClientSize = System::Drawing::Size(292, 273);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->btnWrite);
			this->Controls->Add(this->linkLabel1);
			this->Controls->Add(this->lblText);
			this->Controls->Add(this->btnShowTxt);
			this->Controls->Add(this->btnExit);
			this->Name = L"Form1";
			this->Text = L"Yay App";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void btnExit_Click(System::Object^  sender, System::EventArgs^  e) {

				 Close();
			 }
	private: System::Void btnShowTxt_Click(System::Object^  sender, System::EventArgs^  e) {

				 static int i=0;
				 i++;

				 lblText->Text="Visual C++";
				 btnShowTxt->Text="Hide Label";
				 if ((i%2)==0)
				 {lblText->Text="";
				 btnShowTxt->Text="Show Label";
				 };

			 }
	private: System::Void linkLabel1_LinkClicked(System::Object^  sender, System::Windows::Forms::LinkLabelLinkClickedEventArgs^  e) {

				 System::Diagnostics::Process::Start(linkLabel1->Text);

				 linkLabel1->LinkVisited=true;
			 }
	private: System::Void btnWrite_Click(System::Object^  sender, System::EventArgs^  e) {


				 label1->Text=(textBox2->Text);

				 if (textBox2->Text=="") 
				 {

					 textBox1->AppendText("Artur's First App i VC++! Yay!"+ System::Environment::NewLine);
				 }

				 else
				 {
					 textBox1->AppendText(textBox2->Text + System::Environment::NewLine);

				 };


			 }
	private: System::Void label3_Click(System::Object^  sender, System::EventArgs^  e) {
			 }


	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

				 if (textBox3->Text!="")

				 {System::Int16 ShowLine = System::Int16::Parse(textBox3->Text);

				 if (ShowLine < textBox1->Lines->Length)
					 label2->Text = textBox1->Lines[ShowLine];

				 if (ShowLine > textBox1->Lines->Length)
					 label2->Text="There is No such Line number :P";
				 };

			 }
	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {

				 textBox1->Clear();
			 }
	};
}

