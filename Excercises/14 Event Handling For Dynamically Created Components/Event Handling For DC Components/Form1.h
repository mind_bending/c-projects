#pragma once

namespace EventHandlingForDCComponents {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(150, 30);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(91, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Click Me!";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 61);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(0, 13);
			this->label1->TabIndex = 1;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(10, 6);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(354, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"This is a presentation of Event Handling for dynamically Created Windows";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(427, 92);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button1);
			this->MaximumSize = System::Drawing::Size(443, 130);
			this->MinimumSize = System::Drawing::Size(443, 130);
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Event Handling For Dynamically Created Components";
			this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Form1::okno_FormClosed);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: System::Void przycisk_Click(System::Object^  sender, System::EventArgs^  e) {
			 
			 Label^ etykieta = gcnew Label;
			 etykieta->Width=250;
			 etykieta->Text="Some Text in Some Window";
			 etykieta->Location=Point(100,100);
			 ((Button^)sender)->Parent->Controls->Add(etykieta);
			 label1->Text="You Made It!";

			 }


	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

				 Form^ okno = gcnew Form;
				 okno->Text="Some Window";
				 Button^ przycisk = gcnew Button;
				 przycisk->Width=200;
				 przycisk->Text="Click Me Too!";
				 okno->Controls->Add(przycisk);
				 przycisk->Click += gcnew System::EventHandler(this, &Form1::przycisk_Click);
				 okno->Show();
				 okno->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Form1::okno_FormClosed);
			 }

	private: System::Void okno_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {

				 label1->Text="NOoo You Closed The Window, Ahh Curses!...";
			 }
	};
}

