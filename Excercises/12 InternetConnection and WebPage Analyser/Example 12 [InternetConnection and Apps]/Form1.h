#pragma once

namespace Example12InternetConnectionandApps {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::WebBrowser^  webBrowser1;
	protected: 
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::TextBox^  textBox4;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->webBrowser1 = (gcnew System::Windows::Forms::WebBrowser());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// webBrowser1
			// 
			this->webBrowser1->Location = System::Drawing::Point(12, 1);
			this->webBrowser1->MinimumSize = System::Drawing::Size(20, 20);
			this->webBrowser1->Name = L"webBrowser1";
			this->webBrowser1->Size = System::Drawing::Size(681, 418);
			this->webBrowser1->TabIndex = 0;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 454);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(111, 23);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Show WebPage";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(142, 454);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Go Back";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(235, 454);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 25);
			this->button3->TabIndex = 3;
			this->button3->Text = L"Go Forward";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(12, 425);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(668, 20);
			this->textBox1->TabIndex = 4;
			this->textBox1->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Form1::textBox1_KeyDown);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(699, 34);
			this->textBox2->Multiline = true;
			this->textBox2->Name = L"textBox2";
			this->textBox2->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->textBox2->Size = System::Drawing::Size(224, 137);
			this->textBox2->TabIndex = 5;
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(699, 5);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(224, 23);
			this->button4->TabIndex = 6;
			this->button4->Text = L"Show All Images on WebPage";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(699, 177);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(224, 23);
			this->button5->TabIndex = 7;
			this->button5->Text = L"Show All Links on WebPage";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Form1::button5_Click);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(699, 206);
			this->textBox3->Multiline = true;
			this->textBox3->Name = L"textBox3";
			this->textBox3->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->textBox3->Size = System::Drawing::Size(224, 139);
			this->textBox3->TabIndex = 8;
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(699, 351);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(224, 23);
			this->button6->TabIndex = 9;
			this->button6->Text = L"Show Me Cookie!";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &Form1::button6_Click);
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(699, 380);
			this->textBox4->Multiline = true;
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(224, 65);
			this->textBox4->TabIndex = 10;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(925, 489);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->webBrowser1);
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Internet Connection and Applications";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

				 Uri^ adres = gcnew Uri("http://google.pl");
					 webBrowser1->Url=adres;
			 }
	private: System::Void textBox1_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {

				 if (e->KeyData==System::Windows::Forms::Keys::Enter)
					 webBrowser1->Navigate("http://"+textBox1->Text);

			 }
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {

			 webBrowser1->GoBack();
		 }
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {

			 webBrowser1->GoForward();

		 }
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {

			 System::Collections::IEnumerator^ element= webBrowser1->Document->Images->GetEnumerator();
			 element->MoveNext();
			 while ((element->MoveNext())&&(element!=nullptr)){
			 textBox2->AppendText(((HtmlElement^)(element->Current))->GetAttribute("SRC")->ToString());
			 textBox2->AppendText(System::Environment::NewLine);
			 }
		 }
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {

			 System::Collections::IEnumerator^ odnosnik= webBrowser1->Document->Links->GetEnumerator();
			 
			 odnosnik->MoveNext();

			 while ((odnosnik->MoveNext())&&(odnosnik!=nullptr)){
				 
			 //textBox3->AppendText(((HtmlElement^)(odnosnik->Current))->InnerText->ToString()+" ");
			 textBox3->AppendText(((HtmlElement^)(odnosnik->Current))->GetAttribute("href")->ToString());	 
			 textBox3->AppendText(System::Environment::NewLine);
			 }

		 }
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
			 System::String^ cookie;
			 cookie=webBrowser1->Document->Cookie;
			 textBox4->Clear();
			 if (cookie!=nullptr)
				 textBox4->AppendText(cookie);
			 else
				textBox4->AppendText("No cookie for you, No No Fatty!");
		 }
};
}

