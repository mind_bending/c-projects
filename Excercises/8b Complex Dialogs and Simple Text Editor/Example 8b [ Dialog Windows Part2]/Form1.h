#pragma once

namespace Example8bDialogWindowsPart2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	protected: 
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
	private: System::Windows::Forms::ColorDialog^  colorDialog1;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::FontDialog^  fontDialog1;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::CheckBox^  checkBox1;
	private: System::Windows::Forms::CheckBox^  checkBox2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Button^  button7;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Button^  button8;
	private: System::Windows::Forms::Button^  button9;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Button^  button10;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->colorDialog1 = (gcnew System::Windows::Forms::ColorDialog());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->fontDialog1 = (gcnew System::Windows::Forms::FontDialog());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBox2 = (gcnew System::Windows::Forms::CheckBox());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(14, 27);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->textBox1->Size = System::Drawing::Size(447, 271);
			this->textBox1->TabIndex = 0;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 326);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(59, 23);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Open";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(86, 326);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(59, 23);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Save";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// saveFileDialog1
			// 
			this->saveFileDialog1->CreatePrompt = true;
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->aboutToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(573, 24);
			this->menuStrip1->TabIndex = 3;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(40, 20);
			this->aboutToolStripMenuItem->Text = L"Info";
			this->aboutToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::aboutToolStripMenuItem_Click);
			// 
			// colorDialog1
			// 
			this->colorDialog1->AllowFullOpen = false;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(328, 331);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(0, 13);
			this->label1->TabIndex = 5;
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(161, 326);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(59, 23);
			this->button4->TabIndex = 6;
			this->button4->Text = L"Font";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
			// 
			// checkBox1
			// 
			this->checkBox1->AutoSize = true;
			this->checkBox1->Location = System::Drawing::Point(467, 38);
			this->checkBox1->Name = L"checkBox1";
			this->checkBox1->Size = System::Drawing::Size(79, 17);
			this->checkBox1->TabIndex = 7;
			this->checkBox1->Text = L"UpperCase";
			this->checkBox1->UseVisualStyleBackColor = true;
			this->checkBox1->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox1_CheckedChanged);
			// 
			// checkBox2
			// 
			this->checkBox2->AutoSize = true;
			this->checkBox2->Location = System::Drawing::Point(467, 61);
			this->checkBox2->Name = L"checkBox2";
			this->checkBox2->Size = System::Drawing::Size(79, 17);
			this->checkBox2->TabIndex = 8;
			this->checkBox2->Text = L"LowerCase";
			this->checkBox2->UseVisualStyleBackColor = true;
			this->checkBox2->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox1_CheckedChanged);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(486, 93);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 23);
			this->button3->TabIndex = 9;
			this->button3->Text = L"Copy";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(486, 122);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(75, 23);
			this->button5->TabIndex = 10;
			this->button5->Text = L"Cut";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Form1::button5_Click);
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(486, 151);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(75, 23);
			this->button6->TabIndex = 11;
			this->button6->Text = L"Paste";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &Form1::button6_Click);
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(486, 206);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(75, 23);
			this->button7->TabIndex = 12;
			this->button7->Text = L"Add //";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &Form1::button7_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(484, 190);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(77, 13);
			this->label2->TabIndex = 13;
			this->label2->Text = L"Add comments";
			// 
			// button8
			// 
			this->button8->Location = System::Drawing::Point(487, 235);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(75, 23);
			this->button8->TabIndex = 14;
			this->button8->Text = L"Remove //";
			this->button8->UseVisualStyleBackColor = true;
			this->button8->Click += gcnew System::EventHandler(this, &Form1::button8_Click);
			// 
			// button9
			// 
			this->button9->Location = System::Drawing::Point(487, 326);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(75, 23);
			this->button9->TabIndex = 15;
			this->button9->Text = L"Add text line";
			this->button9->UseVisualStyleBackColor = true;
			this->button9->Click += gcnew System::EventHandler(this, &Form1::button9_Click);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(529, 274);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(32, 20);
			this->textBox2->TabIndex = 16;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(487, 300);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(75, 20);
			this->textBox3->TabIndex = 17;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(483, 277);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(47, 13);
			this->label3->TabIndex = 18;
			this->label3->Text = L"Line No:";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(419, 303);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(64, 13);
			this->label4->TabIndex = 19;
			this->label4->Text = L"Text to add:";
			// 
			// button10
			// 
			this->button10->Location = System::Drawing::Point(235, 326);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(59, 23);
			this->button10->TabIndex = 20;
			this->button10->Text = L"Clear";
			this->button10->UseVisualStyleBackColor = true;
			this->button10->Click += gcnew System::EventHandler(this, &Form1::button10_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(573, 361);
			this->Controls->Add(this->button10);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->button9);
			this->Controls->Add(this->button8);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->button7);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->checkBox2);
			this->Controls->Add(this->checkBox1);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"Open/Save File Dialogs ";
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

			openFileDialog1->Filter ="Text files (*.txt)|*.txt|All files (*.*)|*.*";

		if (openFileDialog1->ShowDialog()==System::Windows::Forms::DialogResult::OK)
		
		{
					 
			StreamReader^ file = gcnew StreamReader (openFileDialog1->FileName,System::Text::Encoding::Default);
			textBox1->Text=file->ReadToEnd();
			file->Close();
				
		}
			 
			 }

	private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {

		saveFileDialog1->Filter ="Text files (*.txt)|*.txt|All files (*.*)|*.*";

		if (saveFileDialog1->ShowDialog()==System::Windows::Forms::DialogResult::OK)
		
		{
					 
			StreamWriter^ file = gcnew StreamWriter(saveFileDialog1->FileName);
			file->Write(textBox1->Text);
			file->Close();
				
		}

			 }
private: System::Void aboutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			MessageBox::Show("Author: Artur Wieczorek ", "About This Program", MessageBoxButtons::OK, MessageBoxIcon::Information);
		 }

private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {

			 fontDialog1->MinSize=10;
			 fontDialog1->MaxSize=16;
			 fontDialog1->ShowColor=true;

			 if (fontDialog1->ShowDialog()==System::Windows::Forms::DialogResult::OK){			 
			 textBox1->Font=fontDialog1->Font;
			 textBox1->ForeColor=fontDialog1->Color;
			 label1->Text="You selected " + fontDialog1->Color.Name;
			 label1->ForeColor=fontDialog1->Color;
			 }
		 }


private: System::Void checkBox1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

			 if ((checkBox1->CheckState==CheckState::Checked) & (checkBox2->CheckState==CheckState::Unchecked))
			 {textBox1->CharacterCasing=CharacterCasing::Upper;
			 checkBox2->Enabled=false;
			 }

			 if ((checkBox1->CheckState==CheckState::Unchecked) & (checkBox2->Enabled==false))
			 {
			 checkBox2->Enabled=true;
			 }


			 if ((checkBox2->CheckState==CheckState::Checked) & (checkBox1->CheckState==CheckState::Unchecked))
			 {
				 checkBox1->Enabled=false;
				 textBox1->CharacterCasing=CharacterCasing::Lower;
			 }

			 if ((checkBox2->CheckState==CheckState::Unchecked) & (checkBox1->Enabled==false))
			 {
			 checkBox1->Enabled=true;
			 }
		 }
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {

			 textBox1->Copy();
		 }
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {

			 textBox1->Cut();
		 }
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {

			 textBox1->Paste();
		 }
private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) {

			 array<System::String^>^ linie = gcnew array<System::String^>(textBox1->Lines->Length);
			 
			 linie=textBox1->Lines;
			 textBox1->Clear();
			 

			 for (System::Int32 i=0; i<linie->Length; i++)

			 {	//System::Int32 L;
				//L = linie->Length;

				if (!(linie[i]->Contains("//")))	
				textBox1->AppendText(linie[i]->Insert(0, "//")+System::Environment::NewLine);


				//if (!(linie[i]->Contains("//")) && (i=L))
				//textBox1->AppendText(linie[i]->Insert(0, "//"));

				if (linie[i]->Contains("//"))
				textBox1->AppendText(linie[i]+System::Environment::NewLine);
			 
				}
			
		 }



private: System::Void button8_Click(System::Object^  sender, System::EventArgs^  e) {
			 
			 array<System::String^>^ linie = gcnew array<System::String^>(textBox1->Lines->Length);
			 linie=textBox1->Lines;
			 textBox1->Clear();

			 for (System::Int32 i=0; i<linie->Length; i++)

			 {
			 if (linie[i]->Contains("//"))
			 textBox1->AppendText(linie[i]->Remove(0,2)+System::Environment::NewLine);

			 else
			 textBox1->AppendText(linie[i]+System::Environment::NewLine);
			 
			 }

			 
		 }
////Custom method for adding text line beetween existing lines
private: System::Void dopisz(System::Windows::Forms::TextBox^ pole_text, System::Int16 nrLini, System::String^ text) {
		 
				 if (pole_text->Lines->Length==0)
					 return;
				 if (pole_text->Lines->Length<nrLini)
					 return;

			     array<System::String^>^ linie = gcnew array<System::String^>(pole_text->Lines->Length);
				 linie=pole_text->Lines;
				 pole_text->Clear();

				 for (System::Int32 i=0; i<nrLini;i++)
					pole_text->AppendText(linie[i]+System::Environment::NewLine);
				
				 pole_text->AppendText(text+System::Environment::NewLine);

				 for (System::Int32 i=nrLini; i<linie->Length;i++)
					pole_text->AppendText(linie[i]+System::Environment::NewLine);
		 }

////End of custom methods

private: System::Void button9_Click(System::Object^  sender, System::EventArgs^  e) {
			 
			 System::Int16 lineNo;
			 lineNo = Convert::ToInt16(textBox2->Text);
			 System::String^ textToAdd;
			 textToAdd=textBox3->Text;
			 dopisz(textBox1,lineNo, textToAdd);
		 
		 
		 }
private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e) {
			 textBox1->Clear();
		 }
};
}

