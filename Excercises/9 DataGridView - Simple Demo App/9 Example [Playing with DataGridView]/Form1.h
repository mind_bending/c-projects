#pragma once

namespace My9ExamplePlayingwithDataGridView {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::DataGridView^  dataGridView1;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Button^  button5;
	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->button5 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView1))->BeginInit();
			this->SuspendLayout();
			// 
			// dataGridView1
			// 
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridView1->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::DisplayedCells;
			this->dataGridView1->Location = System::Drawing::Point(12, 12);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(349, 132);
			this->dataGridView1->TabIndex = 0;
			this->dataGridView1->CurrentCellChanged += gcnew System::EventHandler(this, &Form1::dataGridView1_CurrentCellChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 153);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(0, 13);
			this->label1->TabIndex = 2;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 271);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 3;
			this->button1->Text = L"Go!";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 186);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(0, 13);
			this->label2->TabIndex = 4;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(12, 212);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(0, 13);
			this->label3->TabIndex = 5;
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(180, 153);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(181, 72);
			this->textBox1->TabIndex = 6;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(9, 255);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(145, 13);
			this->label4->TabIndex = 7;
			this->label4->Text = L"Select some row(s) and click ";
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(180, 255);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(105, 23);
			this->button2->TabIndex = 8;
			this->button2->Text = L"Add Row(s)";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(180, 311);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(105, 22);
			this->button3->TabIndex = 9;
			this->button3->Text = L"Remove Last Row";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(322, 255);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(39, 20);
			this->textBox2->TabIndex = 10;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(240, 239);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(121, 13);
			this->label5->TabIndex = 11;
			this->label5->Text = L"How many rows to add\?";
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(180, 284);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(105, 23);
			this->button4->TabIndex = 12;
			this->button4->Text = L"Add Column";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(107, 308);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(28, 20);
			this->textBox3->TabIndex = 13;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(9, 311);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(92, 13);
			this->label6->TabIndex = 14;
			this->label6->Text = L"Row No. To Copy";
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(12, 334);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(123, 23);
			this->button5->TabIndex = 15;
			this->button5->Text = L"Copy and Insert";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Form1::button5_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(373, 362);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->dataGridView1);
			this->Name = L"Form1";
			this->Text = L"DataGridView - Presentation";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {

				 dataGridView1->ColumnCount=5;
				 dataGridView1->RowCount=5;

				 DataGridViewCellStyle^ styl = gcnew DataGridViewCellStyle();
				 System::Drawing::Font^ czcionka = gcnew 
				 System::Drawing::Font(System::Drawing::FontFamily::GenericSansSerif, 11,  FontStyle::Regular);
				 styl->Font=czcionka;
				 styl->ForeColor=System::Drawing::Color::White;
				 dataGridView1->AlternatingRowsDefaultCellStyle=styl;





				 dataGridView1->DefaultCellStyle->BackColor=Color::White;
				 dataGridView1->AlternatingRowsDefaultCellStyle->BackColor=Color::Maroon;
				// dataGridView1->AlternatingRowsDefaultCellStyle->ForeColor=Color::White;

				 for (System::Int32 i=0; i<5; i++)
				 {
					 for (System::Int32 j=0; j<5; j++){

					 dataGridView1->Rows[i]->Cells[j]->Value=j+i;
					 dataGridView1->Columns[j]->HeaderCell->Value=j.ToString();
					 }

					 dataGridView1->Rows[i]->HeaderCell->Value=i.ToString();
				 }

				//for (System::Int32 i=0; i<dataGridView1->ColumnCount; i++)
				//{
				//	dataGridView1->Columns[i]->Width=50;
				//}
				
				//for (System::Int32 i=0; i<dataGridView1->RowCount; i++)
				//{
				//	dataGridView1->Rows[i]->Height=33;
				//}

			 }
	private: System::Void dataGridView1_CurrentCellChanged(System::Object^  sender, System::EventArgs^  e) {

				 dataGridView1->AutoResizeColumns();
			     dataGridView1->AutoResizeRows();
			 }






private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

			 textBox1->Clear();
			 System::Int32 ile_liczb;
			 System::Single suma;
			 
			 ile_liczb=dataGridView1->SelectedCells->Count;
			 label2->Text="You selected: " + ile_liczb.ToString()+ " value(s)";
			 
			 for (System::Int32 i=0; i<ile_liczb;i++)
			 { suma=suma+Convert::ToSingle(dataGridView1->SelectedCells[i]->Value);}

			 label3->Text="Sum of selected cells: "+suma.ToString();

			 array<System::Single>^ sum = gcnew array<System::Single>(dataGridView1->SelectedRows->Count);

			 for (System::Int32 i=0; i<dataGridView1->SelectedRows->Count;i++)
			 {
				for (System::Int32 j=0; j<dataGridView1->ColumnCount;j++)
					sum[i]+=Convert::ToSingle(dataGridView1->SelectedRows[i]->Cells[j]->Value);


			 }

			 
			 

			 for (System::Int32 z=0; z<dataGridView1->SelectedRows->Count;z++)
			 {
				 DataGridViewRow^ wiersze = gcnew DataGridViewRow();
				 wiersze = dataGridView1->SelectedRows[z];
				 textBox1->AppendText("sum of values for row r[" +wiersze->Index+"] "+sum[z].ToString() +System::Environment::NewLine);
				 //System::Environment::NewLine
			 }
		 }



private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
				
			 
			 dataGridView1->Rows->Add(Convert::ToSingle(textBox2->Text));
		 }
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {

			 if (dataGridView1->RowCount!=1)
			 dataGridView1->Rows->RemoveAt(dataGridView1->RowCount-2);
		 }
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {

			 DataGridViewTextBoxCell^ komorka=gcnew DataGridViewTextBoxCell();
			 DataGridViewColumn^ col = gcnew DataGridViewColumn(komorka);
			 col->HeaderCell->Value="New One";

			 dataGridView1->Columns->Add(col);
		 }
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
			 //dataGridView1->Rows->InsertCopy(Convert::ToSingle(textBox3->Text),dataGridView1->RowCount-1);
			 dataGridView1->Rows->AddCopy(Convert::ToInt32(textBox3->Text));

		 }
};
}

