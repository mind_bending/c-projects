========================================================================
    APPLICATION : Thread and BackgroundWorker Component Project Overview
========================================================================

AppWizard has created this Thread and BackgroundWorker Component Application for you.  

This file contains a summary of what you will find in each of the files that
make up your Thread and BackgroundWorker Component application.

Thread and BackgroundWorker Component.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard. 
    It contains information about the version of Visual C++ that generated the file, and 
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

Thread and BackgroundWorker Component.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).

Thread and BackgroundWorker Component.cpp
    This is the main application source file.
    Contains the code to display the form.

Form1.h
    Contains the implementation of your form class and InitializeComponent() function.

AssemblyInfo.cpp
    Contains custom attributes for modifying assembly metadata.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named Thread and BackgroundWorker Component.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
