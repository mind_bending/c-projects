#pragma once

namespace My5bExamleDynamicArrays {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  textBox1;
	protected: 
	private: System::Windows::Forms::Button^  button1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(12, 12);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(268, 254);
			this->textBox1->TabIndex = 0;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(106, 272);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Show!";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(293, 303);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox1);
			this->Name = L"Form1";
			this->Text = L"Dynamic Arrays";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

				 array <System::Single>^ tablica1  = gcnew array<System::Single>(4);
				 array <System::Single,2>^ tablica2  = gcnew array<System::Single,2>(4,5);

				 for (int n=0;n<4;n++){
				 
				 tablica1[n]=n; 
				 tablica2[n,n]=n;}

				 textBox1->AppendText("Array 1 has "+ tablica1->Rank +" dimension" + System::Environment::NewLine);
				 textBox1->AppendText("and contains "+ tablica1->Length +" elements" + System::Environment::NewLine);
				 textBox1->AppendText(System::Environment::NewLine);
				 textBox1->AppendText("Array 1 elements: "+ System::Environment::NewLine);

				 for (int n=0;n<4;n++)
					textBox1->AppendText(tablica1[n].ToString()+ System::Environment::NewLine);


				 textBox1->AppendText(System::Environment::NewLine);
				 textBox1->AppendText("Array 2 has "+ tablica2->Rank +" dimension" + System::Environment::NewLine);
				 textBox1->AppendText("and contains "+ tablica2->Length +" elements" + System::Environment::NewLine);
				 textBox1->AppendText(System::Environment::NewLine);
				 textBox1->AppendText("Array 2 elements: "+ System::Environment::NewLine);

				 for (int n=0;n<4;n++){
					 for (int k=0;k<5;k++)
					textBox1->AppendText(tablica2[n,k].ToString());
					textBox1->AppendText(System::Environment::NewLine);
				 }


			 }
	};
}

