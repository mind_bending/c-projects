#pragma once

namespace DynamicWindowCreation2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: Form^ tytul;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::ComponentModel::IContainer^  components;
	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(231, 57);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Click Me!";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(98, 25);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(0, 13);
			this->label1->TabIndex = 1;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(98, 67);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(0, 13);
			this->label2->TabIndex = 2;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(7, 25);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(71, 13);
			this->label3->TabIndex = 3;
			this->label3->Text = L"Text Entered:";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(7, 67);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(53, 13);
			this->label4->TabIndex = 4;
			this->label4->Text = L"Checked:";
			// 
			// timer1
			// 
			this->timer1->Interval = 5000;
			this->timer1->Tick += gcnew System::EventHandler(this, &Form1::timer1_Tick);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(371, 113);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button1);
			this->MaximumSize = System::Drawing::Size(387, 151);
			this->MinimumSize = System::Drawing::Size(387, 151);
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Components in Dynamically Created Window";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

				 Form^ okno = gcnew Form;
				 Label^ etykieta = gcnew Label;
				 etykieta->Text="Enter Some Text Below";
				 TextBox^ pole_tekst = gcnew TextBox;
				 pole_tekst->Location=Point(5,25);
				 pole_tekst->Width=190;
				 Button^ przycisk = gcnew Button;
				 przycisk->Text="OK";
				 przycisk->Location=Point(5,80);
				 CheckBox^ wybor = gcnew CheckBox;
				 wybor->Location=Point(5,50);
				 okno->Text="Sending Data From Dialog Window";
				 okno->Controls->Add(etykieta);
				 okno->Controls->Add(pole_tekst);
				 okno->Controls->Add(przycisk);
				 okno->Controls->Add(wybor);
				 okno->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				 przycisk->DialogResult = System::Windows::Forms::DialogResult::OK;
				 okno->ShowDialog();
				 label1->Text=pole_tekst->Text;
				 label2->Text=wybor->Checked.ToString();
			 }
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {

				 tytul = gcnew Form;
				 tytul->Height=100;
				 tytul->Width=800;
				 tytul->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				 tytul->ControlBox=false;
				 Label^ napis = gcnew Label;
				 System::Drawing::Font^ czcionka = gcnew System::Drawing::Font(System::Drawing::FontFamily::GenericSansSerif,
					 14, FontStyle::Regular);
				 napis->Text="Data Sending From Dialog Window - Presentation; Author: Artur Wieczorek";
				 napis->Font=czcionka;
				 napis->AutoSize=true;
				 napis->Location=Point(80,30);
				 tytul->Controls->Add(napis);
				 tytul->TopMost=true;
				 tytul->Show();
				 timer1->Start();


			 }
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
			 timer1->Stop();
			 tytul->Close();

		 }
};
}

