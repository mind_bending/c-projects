#pragma once

namespace My4ExampleMenu {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected: 
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  openToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  closeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  toolsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  pencilToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  rubberToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  brushToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::ToolStripComboBox^  toolStripComboBox1;
	private: System::Windows::Forms::ToolStripTextBox^  toolStripTextBox1;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutThisProgramToolStripMenuItem;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip1;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::ToolStripMenuItem^  option1ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  option2ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem1;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::ToolStripMenuItem^  zoomInToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  zoomOutToolStripMenuItem;
	private: System::Windows::Forms::ToolStrip^  toolStrip1;
	private: System::Windows::Forms::ToolStripButton^  toolStripButton1;
	private: System::Windows::Forms::ToolStripButton^  toolStripButton2;
	private: System::Windows::Forms::ToolStripButton^  toolStripButton3;
	private: System::Windows::Forms::ToolStripComboBox^  toolStripComboBox2;
	private: System::Windows::Forms::ToolStripLabel^  toolStripLabel1;
	private: System::Windows::Forms::ToolStripLabel^  toolStripLabel2;
	private: System::Windows::Forms::ToolStripButton^  toolStripButton4;
	private: System::ComponentModel::IContainer^  components;




	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->contextMenuStrip1 = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->option1ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->option2ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->closeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->pencilToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->rubberToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->brushToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->zoomInToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->zoomOutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutThisProgramToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripComboBox1 = (gcnew System::Windows::Forms::ToolStripComboBox());
			this->toolStripTextBox1 = (gcnew System::Windows::Forms::ToolStripTextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
			this->toolStripButton1 = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton2 = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton3 = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton4 = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripComboBox2 = (gcnew System::Windows::Forms::ToolStripComboBox());
			this->toolStripLabel1 = (gcnew System::Windows::Forms::ToolStripLabel());
			this->toolStripLabel2 = (gcnew System::Windows::Forms::ToolStripLabel());
			this->menuStrip1->SuspendLayout();
			this->contextMenuStrip1->SuspendLayout();
			this->toolStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->ContextMenuStrip = this->contextMenuStrip1;
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->fileToolStripMenuItem, 
				this->toolsToolStripMenuItem, this->helpToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(362, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// contextMenuStrip1
			// 
			this->contextMenuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->option1ToolStripMenuItem, 
				this->option2ToolStripMenuItem, this->exitToolStripMenuItem1});
			this->contextMenuStrip1->Name = L"contextMenuStrip1";
			this->contextMenuStrip1->Size = System::Drawing::Size(153, 92);
			this->contextMenuStrip1->Opening += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::contextMenuStrip1_Opening);
			// 
			// option1ToolStripMenuItem
			// 
			this->option1ToolStripMenuItem->Name = L"option1ToolStripMenuItem";
			this->option1ToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->option1ToolStripMenuItem->Text = L"Option 1";
			this->option1ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::option1ToolStripMenuItem_Click);
			// 
			// option2ToolStripMenuItem
			// 
			this->option2ToolStripMenuItem->Name = L"option2ToolStripMenuItem";
			this->option2ToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->option2ToolStripMenuItem->Text = L"Option 2";
			this->option2ToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::option1ToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem1
			// 
			this->exitToolStripMenuItem1->Name = L"exitToolStripMenuItem1";
			this->exitToolStripMenuItem1->Size = System::Drawing::Size(152, 22);
			this->exitToolStripMenuItem1->Text = L"Exit";
			this->exitToolStripMenuItem1->Click += gcnew System::EventHandler(this, &Form1::exitToolStripMenuItem1_Click);
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->openToolStripMenuItem, 
				this->closeToolStripMenuItem, this->exitToolStripMenuItem});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(35, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// openToolStripMenuItem
			// 
			this->openToolStripMenuItem->Name = L"openToolStripMenuItem";
			this->openToolStripMenuItem->Size = System::Drawing::Size(130, 22);
			this->openToolStripMenuItem->Text = L"Open";
			this->openToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::openToolStripMenuItem_Click);
			// 
			// closeToolStripMenuItem
			// 
			this->closeToolStripMenuItem->Name = L"closeToolStripMenuItem";
			this->closeToolStripMenuItem->Size = System::Drawing::Size(130, 22);
			this->closeToolStripMenuItem->Text = L"Close";
			this->closeToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::closeToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->ShortcutKeyDisplayString = L"Alt + x";
			this->exitToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Alt | System::Windows::Forms::Keys::X));
			this->exitToolStripMenuItem->Size = System::Drawing::Size(130, 22);
			this->exitToolStripMenuItem->Text = L"Exit";
			this->exitToolStripMenuItem->ToolTipText = L"Code is implemented here!";
			this->exitToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::exitToolStripMenuItem_Click);
			// 
			// toolsToolStripMenuItem
			// 
			this->toolsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {this->pencilToolStripMenuItem, 
				this->rubberToolStripMenuItem, this->brushToolStripMenuItem, this->zoomInToolStripMenuItem, this->zoomOutToolStripMenuItem});
			this->toolsToolStripMenuItem->Name = L"toolsToolStripMenuItem";
			this->toolsToolStripMenuItem->Size = System::Drawing::Size(45, 20);
			this->toolsToolStripMenuItem->Text = L"Tools";
			// 
			// pencilToolStripMenuItem
			// 
			this->pencilToolStripMenuItem->Name = L"pencilToolStripMenuItem";
			this->pencilToolStripMenuItem->Size = System::Drawing::Size(163, 22);
			this->pencilToolStripMenuItem->Text = L"Pencil";
			this->pencilToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::openToolStripMenuItem_Click);
			// 
			// rubberToolStripMenuItem
			// 
			this->rubberToolStripMenuItem->Name = L"rubberToolStripMenuItem";
			this->rubberToolStripMenuItem->Size = System::Drawing::Size(163, 22);
			this->rubberToolStripMenuItem->Text = L"Rubber";
			this->rubberToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::openToolStripMenuItem_Click);
			// 
			// brushToolStripMenuItem
			// 
			this->brushToolStripMenuItem->Name = L"brushToolStripMenuItem";
			this->brushToolStripMenuItem->Size = System::Drawing::Size(163, 22);
			this->brushToolStripMenuItem->Text = L"Brush";
			this->brushToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::openToolStripMenuItem_Click);
			// 
			// zoomInToolStripMenuItem
			// 
			this->zoomInToolStripMenuItem->Name = L"zoomInToolStripMenuItem";
			this->zoomInToolStripMenuItem->ShortcutKeyDisplayString = L"Alt + I";
			this->zoomInToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Alt | System::Windows::Forms::Keys::I));
			this->zoomInToolStripMenuItem->Size = System::Drawing::Size(163, 22);
			this->zoomInToolStripMenuItem->Text = L"Zoom In ";
			this->zoomInToolStripMenuItem->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->zoomInToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::zoomInToolStripMenuItem_Click);
			// 
			// zoomOutToolStripMenuItem
			// 
			this->zoomOutToolStripMenuItem->Name = L"zoomOutToolStripMenuItem";
			this->zoomOutToolStripMenuItem->ShortcutKeyDisplayString = L"Alt + O";
			this->zoomOutToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Alt | System::Windows::Forms::Keys::O));
			this->zoomOutToolStripMenuItem->Size = System::Drawing::Size(163, 22);
			this->zoomOutToolStripMenuItem->Text = L"Zoom Out";
			this->zoomOutToolStripMenuItem->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->zoomOutToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::zoomOutToolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this->helpToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->aboutThisProgramToolStripMenuItem, 
				this->toolStripComboBox1, this->toolStripTextBox1});
			this->helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
			this->helpToolStripMenuItem->Size = System::Drawing::Size(41, 20);
			this->helpToolStripMenuItem->Text = L"Help";
			// 
			// aboutThisProgramToolStripMenuItem
			// 
			this->aboutThisProgramToolStripMenuItem->Name = L"aboutThisProgramToolStripMenuItem";
			this->aboutThisProgramToolStripMenuItem->Size = System::Drawing::Size(181, 22);
			this->aboutThisProgramToolStripMenuItem->Text = L"About This Program";
			this->aboutThisProgramToolStripMenuItem->ToolTipText = L"Info About This Program";
			this->aboutThisProgramToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::aboutThisProgramToolStripMenuItem_Click);
			// 
			// toolStripComboBox1
			// 
			this->toolStripComboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"Item 1", L"Item 2", L"Item 3"});
			this->toolStripComboBox1->Name = L"toolStripComboBox1";
			this->toolStripComboBox1->Size = System::Drawing::Size(121, 21);
			this->toolStripComboBox1->Text = L"DropDown List";
			this->toolStripComboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &Form1::openToolStripMenuItem_Click);
			// 
			// toolStripTextBox1
			// 
			this->toolStripTextBox1->Name = L"toolStripTextBox1";
			this->toolStripTextBox1->Size = System::Drawing::Size(100, 20);
			this->toolStripTextBox1->Text = L"TextBox";
			this->toolStripTextBox1->Click += gcnew System::EventHandler(this, &Form1::openToolStripMenuItem_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(8, 132);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(0, 13);
			this->label1->TabIndex = 1;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(8, 109);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(72, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"You selected:";
			// 
			// textBox1
			// 
			this->textBox1->ContextMenuStrip = this->contextMenuStrip1;
			this->textBox1->Location = System::Drawing::Point(11, 187);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(89, 20);
			this->textBox1->TabIndex = 4;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(8, 171);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(137, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"Right click inside of textBox";
			// 
			// toolStrip1
			// 
			this->toolStrip1->Dock = System::Windows::Forms::DockStyle::Right;
			this->toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(7) {this->toolStripButton1, 
				this->toolStripButton2, this->toolStripButton3, this->toolStripButton4, this->toolStripComboBox2, this->toolStripLabel1, this->toolStripLabel2});
			this->toolStrip1->Location = System::Drawing::Point(274, 24);
			this->toolStrip1->Name = L"toolStrip1";
			this->toolStrip1->Size = System::Drawing::Size(88, 249);
			this->toolStrip1->TabIndex = 6;
			this->toolStrip1->Text = L"toolStrip1";
			// 
			// toolStripButton1
			// 
			this->toolStripButton1->BackColor = System::Drawing::Color::Red;
			this->toolStripButton1->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButton1->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripButton1.Image")));
			this->toolStripButton1->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton1->Name = L"toolStripButton1";
			this->toolStripButton1->Size = System::Drawing::Size(85, 20);
			this->toolStripButton1->Text = L"toolStripButton1";
			this->toolStripButton1->Click += gcnew System::EventHandler(this, &Form1::toolStripButton1_Click);
			// 
			// toolStripButton2
			// 
			this->toolStripButton2->BackColor = System::Drawing::Color::Yellow;
			this->toolStripButton2->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButton2->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripButton2.Image")));
			this->toolStripButton2->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton2->Name = L"toolStripButton2";
			this->toolStripButton2->Size = System::Drawing::Size(85, 20);
			this->toolStripButton2->Text = L"toolStripButton2";
			this->toolStripButton2->Click += gcnew System::EventHandler(this, &Form1::toolStripButton1_Click);
			// 
			// toolStripButton3
			// 
			this->toolStripButton3->BackColor = System::Drawing::Color::Lime;
			this->toolStripButton3->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButton3->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripButton3.Image")));
			this->toolStripButton3->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton3->Name = L"toolStripButton3";
			this->toolStripButton3->Size = System::Drawing::Size(85, 20);
			this->toolStripButton3->Text = L"toolStripButton3";
			this->toolStripButton3->Click += gcnew System::EventHandler(this, &Form1::toolStripButton1_Click);
			// 
			// toolStripButton4
			// 
			this->toolStripButton4->BackColor = System::Drawing::Color::Blue;
			this->toolStripButton4->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButton4->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripButton4.Image")));
			this->toolStripButton4->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton4->Name = L"toolStripButton4";
			this->toolStripButton4->Size = System::Drawing::Size(85, 20);
			this->toolStripButton4->Text = L"toolStripButton4";
			this->toolStripButton4->Click += gcnew System::EventHandler(this, &Form1::toolStripButton1_Click);
			// 
			// toolStripComboBox2
			// 
			this->toolStripComboBox2->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"Select Label 1", L"Select Label 2"});
			this->toolStripComboBox2->Name = L"toolStripComboBox2";
			this->toolStripComboBox2->Size = System::Drawing::Size(83, 21);
			// 
			// toolStripLabel1
			// 
			this->toolStripLabel1->Name = L"toolStripLabel1";
			this->toolStripLabel1->Size = System::Drawing::Size(85, 13);
			this->toolStripLabel1->Text = L"Label 1";
			// 
			// toolStripLabel2
			// 
			this->toolStripLabel2->Name = L"toolStripLabel2";
			this->toolStripLabel2->Size = System::Drawing::Size(85, 13);
			this->toolStripLabel2->Text = L"Label 2";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(362, 273);
			this->Controls->Add(this->toolStrip1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Menu Presentation";
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->contextMenuStrip1->ResumeLayout(false);
			this->toolStrip1->ResumeLayout(false);
			this->toolStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				 Close();
			 }
private: System::Void aboutThisProgramToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			
			String^ message = "This is simple presentation of menu by Artur Wieczorek";
			String^ caption = "YaY! app 2.0";
			MessageBoxButtons buttons = MessageBoxButtons::OK;
			

         // Displays the MessageBox.
          MessageBox::Show( this, message, caption, buttons );
		  label1->Text=((ToolStripMenuItem^)sender)->Text; 
			// Displays the MessageBox
			
		 }
private: System::Void openToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			
			 if ((sender->GetType())->Name=="ToolStripMenuItem")
			label1->Text=((ToolStripMenuItem^)sender)->Text; 
			
			 if ((sender->GetType())->Name=="ToolStripComboBox")
			label1->Text=((ToolStripComboBox^)sender)->Text;

			 if ((sender->GetType())->Name=="ToolStripTextBox")
			label1->Text=((ToolStripTextBox^)sender)->Text;

		 }
private: System::Void closeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			label1->Text=((ToolStripMenuItem^)sender)->Text;
			
		 }


private: System::Void contextMenuStrip1_Opening(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {


		 }

private: System::Void option1ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				textBox1->AppendText(((ToolStripMenuItem^)sender)->Text);
		 }
private: System::Void exitToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {

			 Close();
		 }
private: System::Void zoomInToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 
			 this->Height++;
			 this->Width++;
		 }
private: System::Void zoomOutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			
			 this->Height--;
			 this->Width--;
		 }
private: System::Void toolStripButton1_Click(System::Object^  sender, System::EventArgs^  e) {


			 				 if (toolStripComboBox2->SelectedIndex==0){
					
					if (((ToolStripButton^)sender)->Name=="toolStripButton1")
					toolStripLabel1->ForeColor=System::Drawing::Color::Red;

					if (((ToolStripButton^)sender)->Name=="toolStripButton2")
					toolStripLabel1->ForeColor=System::Drawing::Color::Yellow;

					if (((ToolStripButton^)sender)->Name=="toolStripButton3")
					toolStripLabel1->ForeColor=System::Drawing::Color::Green;

					if (((ToolStripButton^)sender)->Name=="toolStripButton4")
					toolStripLabel1->ForeColor=System::Drawing::Color::Blue;
					

		 };


				 if (toolStripComboBox2->SelectedIndex==1){
					
					if (((ToolStripButton^)sender)->Name=="toolStripButton1")
					toolStripLabel2->ForeColor=System::Drawing::Color::Red;

					if (((ToolStripButton^)sender)->Name=="toolStripButton2")
					toolStripLabel2->ForeColor=System::Drawing::Color::Yellow;

					if (((ToolStripButton^)sender)->Name=="toolStripButton3")
					toolStripLabel2->ForeColor=System::Drawing::Color::Green;

					if (((ToolStripButton^)sender)->Name=="toolStripButton4")
					toolStripLabel2->ForeColor=System::Drawing::Color::Blue;
					

		 };


};



};}

