#pragma once

namespace Appprotectedbypassword {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(5, 23);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(275, 78);
			this->label1->TabIndex = 0;
			this->label1->Text = resources->GetString(L"label1.Text");
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(292, 134);
			this->Controls->Add(this->label1);
			this->MaximumSize = System::Drawing::Size(308, 172);
			this->MinimumSize = System::Drawing::Size(308, 172);
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"App protected by password";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {

				 Form^ okno = gcnew Form;
				 okno->Height=150;
				 okno->Width=200;
				 okno->Text="PassWord App";
				 okno->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				 okno->FormClosing+=gcnew FormClosingEventHandler (this, &Form1::okno_FormClosing);
				 TextBox^ haslo = gcnew TextBox;
				 haslo->Location=Point(20,30);
				 haslo->PasswordChar='*';
				 Button^ przycisk = gcnew Button;
				 przycisk->Click+= gcnew EventHandler(this, &Form1::przycisk_Click);
				 przycisk->Location=Point(20,60);
				 Label^ etykieta2 = gcnew Label;
				 etykieta2->Location=Point(20,90);
				 etykieta2->Text="Shhh,Enter: password";
				 etykieta2->Width=200;
				 przycisk->Text="OK";
				 Label^ etykieta = gcnew Label;
				 etykieta->Location=Point(20,10);
				 etykieta->Width=200;
				 etykieta->Text="Enter seekwet password:";
				 okno->Controls->Add(haslo);
				 okno->Controls->Add(przycisk);
				 okno->Controls->Add(etykieta);
				 okno->Controls->Add(etykieta2);
				 okno->ShowDialog();

			 }
	private: System::Void okno_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {

				 if ((((Form^)sender)->Controls[0]->Text)!="password")
					 e->Cancel=true;
				 else
					 e->Cancel=false;

			 }
	private: System::Void przycisk_Click(System::Object^  sender, System::EventArgs^  e) {
			 
			 ((Form^)((Button^)sender)->Parent)->Close();
			 }


	};
}

