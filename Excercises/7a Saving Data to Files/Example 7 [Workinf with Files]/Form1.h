#pragma once

namespace Example7WorkinfwithFiles {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 279);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(118, 27);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Create and Save file";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(12, 66);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(218, 20);
			this->textBox1->TabIndex = 1;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(9, 20);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(242, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Enter path - location where file will be created e.g.";
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(12, 156);
			this->textBox2->Multiline = true;
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(331, 117);
			this->textBox2->TabIndex = 3;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(9, 130);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(196, 13);
			this->label2->TabIndex = 4;
			this->label2->Text = L"Enter the content you want to add to file";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(11, 48);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(209, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"C:\\Users\\UserName\\Desktop\\fileName.txt";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(355, 318);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->button1);
			this->Name = L"Form1";
			this->Text = L"Saving data to Files [Artur Wieczorek]";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

				 //System::String^ text;

				 System::String^ fileLocation;
				 System::String^ contentToAdd;
				 fileLocation = textBox1->Text;
				 contentToAdd = textBox2->Text;

				 StreamWriter^ plik= gcnew StreamWriter(fileLocation,1 , System::Text::Encoding::Default);
				 //plik->WriteLine("Linia");
				 //plik->Write("Napis");
				 //System::Single single_number=47.4;
				 //System::Int32 intiger_number=7;
				 //plik->WriteLine(" "+ single_number +" "+ intiger_number);
				 //plik->Write(single_number.ToString());
				 System::DateTime^ now = System::DateTime::Now;
				 plik->Write("This content was added: {0}", now);
				 plik->WriteLine(System::Environment::NewLine + contentToAdd + System::Environment::NewLine);
				 plik->Close();



			 }
	};
}

