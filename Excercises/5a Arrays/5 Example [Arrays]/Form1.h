#pragma once

namespace My5ExampleArrays {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	System::Boolean is_not_odd(System::Int32 number) {
	if ((number%2)||(number==0)) return false;
	else
	return true;
	}

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	protected: 
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(109, 249);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Run!";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(12, 12);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(268, 231);
			this->textBox1->TabIndex = 1;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(312, 12);
			this->textBox2->Multiline = true;
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(268, 231);
			this->textBox2->TabIndex = 2;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(592, 273);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->button1);
			this->Name = L"Form1";
			this->Text = L"Yay Array! App";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		private: System::Void show(array<System::Int32>^ tab1, Array^ tab2){

				System::Int32 w=0;
				do 
				{
				textBox1->AppendText("tab1"+"["+w+"] ="+tab1[w].ToString()+ "   tab2"+"["+w+"] ="+ tab2->GetValue(w) + System::Environment::NewLine);
				w++;
				}
				while (w<tab1->Length);

				textBox1->AppendText(System::Environment::NewLine);
			 
			 }
	

	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {



				 array <System::Int32>^ tablica1= {0,0,0,0,0,0,0,0,0,0};
				 Array^ tablica2 = Array::CreateInstance(System::Int32::typeid,10);
				 Array^ tablica3 = Array::CreateInstance(System::Int32::typeid,10);
				 for (System::Int32 i=0; i<10; i++)

				 {
					 tablica1[i]=i;
					 tablica2->SetValue(i,i);

				 };
				 textBox1->AppendText("Original Arrays"+ System::Environment::NewLine);
				 show(tablica1, tablica2);

				 System::Array::Reverse(tablica1);
				 System::Array::Copy(tablica1, tablica2, tablica1->Length);

				 textBox1->AppendText("Arrays after 'reverse' operation"+ System::Environment::NewLine);
				 show(tablica1, tablica2);

				Predicate<System::Int32>^ mask = gcnew Predicate<System::Int32>(is_not_odd);
				tablica3 = System::Array::FindAll(tablica1,mask);

				System::Collections::IEnumerator^ enum1 = tablica1->GetEnumerator();
				System::Collections::IEnumerator^ enum2 = tablica3->GetEnumerator();

				textBox2->AppendText("Arrays content:"+ System::Environment::NewLine);
				while ((enum1->MoveNext())&&(enum1!=nullptr))
					textBox2->AppendText(enum1->Current + System::Environment::NewLine);

				textBox2->AppendText("Even numbers:" + System::Environment::NewLine);
				while ((enum2->MoveNext())&&(enum2!=nullptr))
					textBox2->AppendText(enum2->Current + System::Environment::NewLine);



			 }
	};
}

